import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import SplashScreen from '../screens/splash/SplashScreen';
import HomeScreen from '../screens/home/HomeScreen';
import LoginScreen from '../screens/login/LoginScreen';
import AboutScreen from '../screens/topDrawer/about/AboutScreen';
import PrivacyPolicy from '../screens/topDrawer/privacyPolicy/PrivacyPolicy';
import FruitsCategory from '../screens/categories/fruits/listCategory/FruitsCategory';
import VegetablesCategory from '../screens/categories/vegetables/listCategory/VegetablesCategory';
import AnimalsCategory from '../screens/categories/animals/listCategory/AnimalsCategory';
import VehiclesCategory from '../screens/categories/vehicles/listCategory/VehiclesCategory';
import SelectOption from '../global/selectOption/SelectOption';

import AppleNumber from '../screens/categories/fruits/apple/number/AppleNumber';
import AppleColor from '../screens/categories/fruits/apple/color/AppleColor';
import BananaNumber from '../screens/categories/fruits/banana/number/BananaNumber';
import BananaColor from '../screens/categories/fruits/banana/color/BananaColor';
import MangoNumber from '../screens/categories/fruits/mango/number/MangoNumber';
import MangoColor from '../screens/categories/fruits/mango/color/MangoColor';
import OrangeNumber from '../screens/categories/fruits/orange/number/OrangeNumber';
import OrangeColor from '../screens/categories/fruits/orange/color/OrangeColor';
import StrawberryNumber from '../screens/categories/fruits/strawBerry/number/StrawberryNumber';
import StrawberryColor from '../screens/categories/fruits/strawBerry/color/StrawberryColor';
import GrapeNumber from '../screens/categories/fruits/grapes/number/GrapeNumber';
import GrapeColor from '../screens/categories/fruits/grapes/color/GrapesColor';
import GuavaNumber from '../screens/categories/fruits/guava/number/GuavaNumber';
import GuavaColor from '../screens/categories/fruits/guava/color/GuavaColor';
import PineappleNumber from '../screens/categories/fruits/pineApple/number/PineappleNumber';
import PineappleColor from '../screens/categories/fruits/pineApple/color/PineappleColor';
import LemonNumber from '../screens/categories/fruits/lemon/number/LemonNumber';
import LemonColor from '../screens/categories/fruits/lemon/color/LemonColor';
import WatermelonNumber from '../screens/categories/fruits/waterMelon/number/WatermelonNumber';
import WatermelonColor from '../screens/categories/fruits/waterMelon/color/WatermelonColor';

import TomatoNumber from '../screens/categories/vegetables/tomato/number/TomatoNumber';
import TomatoColor from '../screens/categories/vegetables/tomato/color/TomatoColor';
import CarrotNumber from '../screens/categories/vegetables/carrot/number/CarrotNumber';
import CarrotColor from '../screens/categories/vegetables/carrot/color/CarrotColor';
import PotatoNumber from '../screens/categories/vegetables/potato/number/PotatoNumber';
import PotatoColor from '../screens/categories/vegetables/potato/color/PotatoColor';
import BrinjalNumber from '../screens/categories/vegetables/brinjal/number/BrinjalNumber';
import BrinjalColor from '../screens/categories/vegetables/brinjal/color/BrinjalColor';
import ChilliNumber from '../screens/categories/vegetables/chilli/number/ChilliNumber';
import ChilliColor from '../screens/categories/vegetables/chilli/color/ChilliColor';
import CauliflowerNumber from '../screens/categories/vegetables/cauliFlower/number/CauliflowerNumber';
import CauliflowerColor from '../screens/categories/vegetables/cauliFlower/color/CaulliflowerColor';
import PumpkinNumber from '../screens/categories/vegetables/pumpkin/number/PumpkinNumber';
import PumpkinColor from '../screens/categories/vegetables/pumpkin/color/PumpkinColor';
import CabbageNumber from '../screens/categories/vegetables/cabbage/number/CabbageNumber';
import CabbageColor from '../screens/categories/vegetables/cabbage/color/CabbageColor';
import OnionNumber from '../screens/categories/vegetables/onion/number/OnionNumber';
import OnionColor from '../screens/categories/vegetables/onion/color/OnionColor';
import CucumberNumber from '../screens/categories/vegetables/cucmber/number/CucumberNumber';
import CucumberColor from '../screens/categories/vegetables/cucmber/color/CucumberColor';

import LionNumber from '../screens/categories/animals/lion/number/LionNumber';
import LionColor from '../screens/categories/animals/lion/color/LionColor';
import ElephantNumber from '../screens/categories/animals/elephant/number/ElephantNumber';
import ElephantColor from '../screens/categories/animals/elephant/color/ElephantColor';
import CowNumber from '../screens/categories/animals/cow/number/CowNumber';
import CowColor from '../screens/categories/animals/cow/color/CowColor';
import DogNumber from '../screens/categories/animals/dog/number/DogNumber';
import DogColor from '../screens/categories/animals/dog/color/DogColor';
import BearNumber from '../screens/categories/animals/bear/number/BearNumber';
import BearColor from '../screens/categories/animals/bear/color/BearColor';
import BullNumber from '../screens/categories/animals/bull/number/BullNumber';
import BullColor from '../screens/categories/animals/bull/color/BullColor';
import MonkeyNumber from '../screens/categories/animals/monkey/number/MonkeyNumber';
import MonkeyColor from '../screens/categories/animals/monkey/color/MonkeyColor';
import HorseNumber from '../screens/categories/animals/horse/number/HorseNumber';
import HorseColor from '../screens/categories/animals/horse/color/HorseColor';
import TortoiseNumber from '../screens/categories/animals/tortoise/number/TortoiseNumber';
import TortoiseColor from '../screens/categories/animals/tortoise/color/TortoiseColor';
import DeerNumber from '../screens/categories/animals/deer/number/DeerNumber';
import DeerColor from '../screens/categories/animals/deer/color/DeerColor';

import CarNumber from '../screens/categories/vehicles/car/number/CarNumber';
import CarColor from '../screens/categories/vehicles/car/color/CarColor';
import LorryNumber from '../screens/categories/vehicles/lorry/number/LorryNumber';
import LorryColor from '../screens/categories/vehicles/lorry/color/LorryColor';
import HelicopterNumber from '../screens/categories/vehicles/helicopter/number/HelicopterNumber';
import HelicopterColor from '../screens/categories/vehicles/helicopter/color/HelicopterColor';
import TrainNumber from '../screens/categories/vehicles/train/number/TrainNumber';
import TrainColor from '../screens/categories/vehicles/train/color/TrainColor';
import BusNumber from '../screens/categories/vehicles/bus/number/BusNumber';
import BusColor from '../screens/categories/vehicles/bus/color/BusColor';
import BikeNumber from '../screens/categories/vehicles/bike/number/BikeNumber';
import BikeColor from '../screens/categories/vehicles/bike/color/BikeColor';
import AutoNumber from '../screens/categories/vehicles/auto/number/AutoNumber';
import AutoColor from '../screens/categories/vehicles/auto/color/AutoColor';
import BoatNumber from '../screens/categories/vehicles/boat/number/BoatNumber';
import BoatColor from '../screens/categories/vehicles/boat/color/BoatColor';
import JcbNumber from '../screens/categories/vehicles/jcb/number/JcbNumber';
import JcbColor from '../screens/categories/vehicles/jcb/color/JcbColor';
import VanNumber from '../screens/categories/vehicles/van/number/VanNumber';
import VanColor from '../screens/categories/vehicles/van/color/VanColor';

import AppleOption from '../screens/categories/fruits/selectOptions/AppleOption';
import BananaOption from '../screens/categories/fruits/selectOptions/BananaOption';
import OrangeOption from '../screens/categories/fruits/selectOptions/OrangeOption';
import MangoOption from '../screens/categories/fruits/selectOptions/MangoOption';
import StrawberryOption from '../screens/categories/fruits/selectOptions/StrawberryOption';
import GrapeOption from '../screens/categories/fruits/selectOptions/GrapesOption';
import GuavaOption from '../screens/categories/fruits/selectOptions/GuavaOption';
import PineappleOption from '../screens/categories/fruits/selectOptions/PineappleOption';
import LemonOption from '../screens/categories/fruits/selectOptions/LemonOption';
import WatermelonOption from '../screens/categories/fruits/selectOptions/WatermelonOption';

import TomatoOption from '../screens/categories/vegetables/selectOptions/TomatoOption';
import CarrotOption from '../screens/categories/vegetables/selectOptions/CarrotOption';
import BrinjalOption from '../screens/categories/vegetables/selectOptions/BrinjalOption';
import PotatoOption from '../screens/categories/vegetables/selectOptions/PotatoOption';
import ChilliOption from '../screens/categories/vegetables/selectOptions/ChilliOption';
import CauliflowerOption from '../screens/categories/vegetables/selectOptions/CauliflowerOption';
import PumpkinOption from '../screens/categories/vegetables/selectOptions/PumpkinOption';
import CabbageOption from '../screens/categories/vegetables/selectOptions/CabbageOption';
import OnionOption from '../screens/categories/vegetables/selectOptions/OnionOption';
import CucumberOption from '../screens/categories/vegetables/selectOptions/CucumberOption';

import CowOption from '../screens/categories/animals/selectOptions/CowOption';
import LionOption from '../screens/categories/animals/selectOptions/LionOption';
import DogOption from '../screens/categories/animals/selectOptions/DogOption';
import ElephantOption from '../screens/categories/animals/selectOptions/ElephantOption';
import BearOption from '../screens/categories/animals/selectOptions/BearOption';
import BullOption from '../screens/categories/animals/selectOptions/BullOption';
import DeerOption from '../screens/categories/animals/selectOptions/DeerOption';
import MonkeyOption from '../screens/categories/animals/selectOptions/MonkeyOption';
import HorseOption from '../screens/categories/animals/selectOptions/HorseOption';
import TortoiseOption from '../screens/categories/animals/selectOptions/TortoiseOption';

import CarOption from '../screens/categories/vehicles/selectOptions/CarOption';
import TrainOption from '../screens/categories/vehicles/selectOptions/TrainOption';
import LorryOption from '../screens/categories/vehicles/selectOptions/LorryOption';
import HelicopterOption from '../screens/categories/vehicles/selectOptions/HelicopterOption';
import AutoOption from '../screens/categories/vehicles/selectOptions/AutoOption';
import BikeOption from '../screens/categories/vehicles/selectOptions/BikeOption';
import BusOption from '../screens/categories/vehicles/selectOptions/BusOption';
import JcbOption from '../screens/categories/vehicles/selectOptions/JcbOption';
import VanOption from '../screens/categories/vehicles/selectOptions/VanOption';
import BoatOption from '../screens/categories/vehicles/selectOptions/BoatOption';

const Stack = createStackNavigator();

function MyStack() {
    return (
        <Stack.Navigator initialRouteName="SplashScreen"
            screenOptions={{
                headerShown: false,
            }}>
            <Stack.Screen name='SplashScreen' component={SplashScreen} />
            <Stack.Screen name='LoginScreen' component={LoginScreen} />
            <Stack.Screen name='HomeScreen' component={HomeScreen} />
            <Stack.Screen name='AboutScreen' component={AboutScreen} />
            <Stack.Screen name='PrivacyPolicy' component={PrivacyPolicy} />
            <Stack.Screen name='FruitsCategory' component={FruitsCategory} />
            <Stack.Screen name='VegetablesCategory' component={VegetablesCategory} />
            <Stack.Screen name='AnimalsCategory' component={AnimalsCategory} />
            <Stack.Screen name='VehiclesCategory' component={VehiclesCategory} />
            <Stack.Screen name='SelectOption' component={SelectOption} />

            {/* Options */}
            <Stack.Screen name='AppleOption' component={AppleOption} />
            <Stack.Screen name='BananaOption' component={BananaOption} />
            <Stack.Screen name='MangoOption' component={MangoOption} />
            <Stack.Screen name='OrangeOption' component={OrangeOption} />
            <Stack.Screen name='StrawberryOption' component={StrawberryOption} />
            <Stack.Screen name='PineappleOption' component={PineappleOption} />
            <Stack.Screen name='GrapeOption' component={GrapeOption} />
            <Stack.Screen name='GuavaOption' component={GuavaOption} />
            <Stack.Screen name='WatermelonOption' component={WatermelonOption} />
            <Stack.Screen name='LemonOption' component={LemonOption} />

            <Stack.Screen name='TomatoOption' component={TomatoOption} />
            <Stack.Screen name='CarrotOption' component={CarrotOption} />
            <Stack.Screen name='BrinjalOption' component={BrinjalOption} />
            <Stack.Screen name='PotatoOption' component={PotatoOption} />
            <Stack.Screen name='ChilliOption' component={ChilliOption} />
            <Stack.Screen name='CauliflowerOption' component={CauliflowerOption} />
            <Stack.Screen name='PumpkinOption' component={PumpkinOption} />
            <Stack.Screen name='CabbageOption' component={CabbageOption} />
            <Stack.Screen name='OnionOption' component={OnionOption} />
            <Stack.Screen name='CucumberOption' component={CucumberOption} />

            <Stack.Screen name='CowOption' component={CowOption} />
            <Stack.Screen name='LionOption' component={LionOption} />
            <Stack.Screen name='DogOption' component={DogOption} />
            <Stack.Screen name='ElephantOption' component={ElephantOption} />
            <Stack.Screen name='BearOption' component={BearOption} />
            <Stack.Screen name='BullOption' component={BullOption} />
            <Stack.Screen name='DeerOption' component={DeerOption} />
            <Stack.Screen name='MonkeyOption' component={MonkeyOption} />
            <Stack.Screen name='HorseOption' component={HorseOption} />
            <Stack.Screen name='TortoiseOption' component={TortoiseOption} />

            <Stack.Screen name='CarOption' component={CarOption} />
            <Stack.Screen name='TrainOption' component={TrainOption} />
            <Stack.Screen name='LorryOption' component={LorryOption} />
            <Stack.Screen name='HelicopterOption' component={HelicopterOption} />
            <Stack.Screen name='AutoOption' component={AutoOption} />
            <Stack.Screen name='BikeOption' component={BikeOption} />
            <Stack.Screen name='BusOption' component={BusOption} />
            <Stack.Screen name='JcbOption' component={JcbOption} />
            <Stack.Screen name='VanOption' component={VanOption} />
            <Stack.Screen name='BoatOption' component={BoatOption} />

            {/* fruits */}
            <Stack.Screen name='AppleNumber' component={AppleNumber} />
            <Stack.Screen name='AppleColor' component={AppleColor} />
            <Stack.Screen name='BananaNumber' component={BananaNumber} />
            <Stack.Screen name='BananaColor' component={BananaColor} />
            <Stack.Screen name='MangoNumber' component={MangoNumber} />
            <Stack.Screen name='MangoColor' component={MangoColor} />
            <Stack.Screen name='OrangeNumber' component={OrangeNumber} />
            <Stack.Screen name='OrangeColor' component={OrangeColor} />
            <Stack.Screen name='StrawberryNumber' component={StrawberryNumber} />
            <Stack.Screen name='StrawberryColor' component={StrawberryColor} />
            <Stack.Screen name='WatermelonNumber' component={WatermelonNumber} />
            <Stack.Screen name='WatermelonColor' component={WatermelonColor} />
            <Stack.Screen name='GuavaNumber' component={GuavaNumber} />
            <Stack.Screen name='GuavaColor' component={GuavaColor} />
            <Stack.Screen name='GrapeNumber' component={GrapeNumber} />
            <Stack.Screen name='GrapeColor' component={GrapeColor} />
            <Stack.Screen name='PineappleNumber' component={PineappleNumber} />
            <Stack.Screen name='PineappleColor' component={PineappleColor} />
            <Stack.Screen name='LemonNumber' component={LemonNumber} />
            <Stack.Screen name='LemonColor' component={LemonColor} />


            {/* vegetables */}
            <Stack.Screen name='TomatoNumber' component={TomatoNumber} />
            <Stack.Screen name='TomatoColor' component={TomatoColor} />
            <Stack.Screen name='CarrotNumber' component={CarrotNumber} />
            <Stack.Screen name='CarrotColor' component={CarrotColor} />
            <Stack.Screen name='BrinjalNumber' component={BrinjalNumber} />
            <Stack.Screen name='BrinjalColor' component={BrinjalColor} />
            <Stack.Screen name='PotatoNumber' component={PotatoNumber} />
            <Stack.Screen name='PotatoColor' component={PotatoColor} />
            <Stack.Screen name='ChilliNumber' component={ChilliNumber} />
            <Stack.Screen name='ChilliColor' component={ChilliColor} />
            <Stack.Screen name='CauliflowerNumber' component={CauliflowerNumber} />
            <Stack.Screen name='CauliflowerColor' component={CauliflowerColor} />
            <Stack.Screen name='PumpkinNumber' component={PumpkinNumber} />
            <Stack.Screen name='PumpkinColor' component={PumpkinColor} />
            <Stack.Screen name='CabbageNumber' component={CabbageNumber} />
            <Stack.Screen name='CabbageColor' component={CabbageColor} />
            <Stack.Screen name='OnionNumber' component={OnionNumber} />
            <Stack.Screen name='OnionColor' component={OnionColor} />
            <Stack.Screen name='CucumberNumber' component={CucumberNumber} />
            <Stack.Screen name='CucumberColor' component={CucumberColor} />

            {/* vehicles */}
            <Stack.Screen name='LionNumber' component={LionNumber} />
            <Stack.Screen name='LionColor' component={LionColor} />
            <Stack.Screen name='CowNumber' component={CowNumber} />
            <Stack.Screen name='CowColor' component={CowColor} />
            <Stack.Screen name='ElephantNumber' component={ElephantNumber} />
            <Stack.Screen name='ElephantColor' component={ElephantColor} />
            <Stack.Screen name='DogNumber' component={DogNumber} />
            <Stack.Screen name='DogColor' component={DogColor} />
            <Stack.Screen name='BearNumber' component={BearNumber} />
            <Stack.Screen name='BearColor' component={BearColor} />
            <Stack.Screen name='BullNumber' component={BullNumber} />
            <Stack.Screen name='BullColor' component={BullColor} />
            <Stack.Screen name='MonkeyNumber' component={MonkeyNumber} />
            <Stack.Screen name='MonkeyColor' component={MonkeyColor} />
            <Stack.Screen name='HorseNumber' component={HorseNumber} />
            <Stack.Screen name='HorseColor' component={HorseColor} />
            <Stack.Screen name='TortoiseNumber' component={TortoiseNumber} />
            <Stack.Screen name='TortoiseColor' component={TortoiseColor} />
            <Stack.Screen name='DeerNumber' component={DeerNumber} />
            <Stack.Screen name='DeerColor' component={DeerColor} />

            {/* vehicles */}
            <Stack.Screen name='CarNumber' component={CarNumber} />
            <Stack.Screen name='CarColor' component={CarColor} />
            <Stack.Screen name='LorryNumber' component={LorryNumber} />
            <Stack.Screen name='LorryColor' component={LorryColor} />
            <Stack.Screen name='TrainNumber' component={TrainNumber} />
            <Stack.Screen name='TrainColor' component={TrainColor} />
            <Stack.Screen name='HelicopterNumber' component={HelicopterNumber} />
            <Stack.Screen name='HelicopterColor' component={HelicopterColor} />
            <Stack.Screen name='AutoNumber' component={AutoNumber} />
            <Stack.Screen name='AutoColor' component={AutoColor} />
            <Stack.Screen name='BusNumber' component={BusNumber} />
            <Stack.Screen name='BusColor' component={BusColor} />
            <Stack.Screen name='BikeNumber' component={BikeNumber} />
            <Stack.Screen name='BikeColor' component={BikeColor} />
            <Stack.Screen name='BoatNumber' component={BoatNumber} />
            <Stack.Screen name='BoatColor' component={BoatColor} />
            <Stack.Screen name='JcbNumber' component={JcbNumber} />
            <Stack.Screen name='JcbColor' component={JcbColor} />
            <Stack.Screen name='VanNumber' component={VanNumber} />
            <Stack.Screen name='VanColor' component={VanColor} />

        </Stack.Navigator>
    )
}

export default StackNavigator = () => {
    return (
        <NavigationContainer>
            <MyStack />
        </NavigationContainer>
    )
}