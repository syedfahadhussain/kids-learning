export default Colors = ({
    Login_Text: '#004170',
    Transparent: '#rgba(0,0,0,0.5)',
    Modal_Text_Color: '#7B2700',
    Modal_Input: '#E68842',
    Header_Text: '#00243E',
    White: '#fff',
    Black: '#000',
    Content_Text: '#B0481A',
})