export default Strings = ({
    welcome: 'Welcome',
    loginText: ' This is a funny game customized for kids to develop their skills in numbers and colors. The kids will enjoy and experience the vibrant colors and interactive easy-to-use design. Let the game mode on.',
    modalText: 'Nick Name',
    menu: 'Menu',
    about: 'About'
})