import Colors from "../../res/colors/Colors"
import { Platform } from "react-native"

export default Styles = ({
    background: {
        flex: 1,
    },
    headerView: {
        // flex: 0.1,
        ...Platform.select({
            ios: {
                flex: 0.1,
                // backgroundColor:'#000',
                marginTop:'6%'
            },
            android: {
                flex: 0.1,
            },
        }),
        flexDirection: 'row',
        justifyContent:'center',
    },
    menuView: {
        flex: 0.2,
        alignItems: 'center',
        justifyContent: 'center',
    },
    textView: {
        flex: 0.9,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        right: '5%',
    },
    text: {
        fontFamily: 'Poppins-SemiBold',
        fontSize: 20,
        color: Colors.Header_Text
    },
    user: {
        fontFamily: 'Poppins-SemiBold',
        fontSize: 24,
        color: Colors.Header_Text,
    },
    textView2: {
        flex: 0.9,
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: '10%',
    },
    text2: {
        fontFamily: 'Poppins-SemiBold',
        fontSize: 20,
        color: Colors.Header_Text
    },
    user2: {
        // top:'-%',
        fontFamily: 'Poppins-SemiBold',
        fontSize: 24,
        color: Colors.Header_Text,
    },
    contentView: {
        flex: 0.9,
    },
    imageView: {
        alignItems: 'center',
        marginBottom: '2%',
        // marginHorizontal:'10%',
        flex: 1,
    },

    centeredView: {
        flex: 1,
        backgroundColor: Colors.Transparent,
    },
    modalBackground: {
        flex: 0.3,
    },

})