import { View, Text, Pressable, Image, ImageBackground, ScrollView, Modal, BackHandler, Alert } from 'react-native'
import React, { useState, useEffect } from 'react'
import AsyncStorage from '@react-native-async-storage/async-storage'
import Images from '../../res/images/Images'
import Styles from './Styles'
import Strings from '../../res/strings/Strings'
import CustomNavigation from '../../global/topNavigation/CustomNavigation'
import { useNavigation } from '@react-navigation/native'
import GlobalImageStyles from '../../global/Styles/GlobalImageStyles'
import LogoutAlert from '../../components/logoutAlert/LogoutAlert'


const HomeScreen = props => {
  const [modalVisible, setModalVisible] = useState(false);
  const [name, setName] = useState('')
  const [Logout, setLogout] = useState(false)

  const navigation = useNavigation()

  useEffect(() => {
    getData()
  }, [])

  useEffect(() => {
    const backAction = () => {
        setLogout(true)
      return true;
    }

    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backAction,
    );

    return () => backHandler.remove();
  }, []);

  const getData = () => {
    try {
      AsyncStorage.getItem('UserName')
      // console.log(name)
        .then(value => {
          if (value != null) {
            setName(value);
          }
        })
    } catch (error) {
      console.log(error);
    }
  }

  const Fruits = () => {
    navigation.navigate('FruitsCategory')
  }
  const Vegetables = () => {
    navigation.navigate('VegetablesCategory')
  }
  const Animals = () => {
    navigation.navigate('AnimalsCategory')
  }
  const Vehicles = () => {
    navigation.navigate('VehiclesCategory')
  }

  return (
    <View style={Styles.background}>
      <ImageBackground
        style={Styles.background}
        source={Images.backgroundScreen}>
        <View style={Styles.headerView}>
          <Pressable onPress={() => [setModalVisible(!modalVisible)]} style={Styles.menuView}>
            <Image style={GlobalImageStyles.headerIcon} source={Images.menuIcon} />
          </Pressable>
          {name.length <= 5 ? (
            <View style={Styles.textView}>
              <Text style={Styles.text}> {Strings.welcome}</Text>
              <Text style={Styles.user}> {name} </Text>
            </View>) : (<View style={Styles.textView2}>
              <Text style={Styles.text2}> {Strings.welcome}</Text>
              <Text style={Styles.user2}> {name} </Text>
            </View>)
          }
        </View>
        <View style={Styles.contentView}>
          <ScrollView>
          <Pressable onPress={() => Animals()}
              style={Styles.imageView}>
              <Image style={GlobalImageStyles.homeImage} source={Images.animals} />
            </Pressable>
            <Pressable onPress={() => Fruits()}
              style={Styles.imageView}>
              <Image style={GlobalImageStyles.homeImage} source={Images.fruits} />
            </Pressable>
            <Pressable onPress={() => Vegetables()}
              style={Styles.imageView}>
              <Image style={GlobalImageStyles.homeImage} source={Images.vegetables} />
            </Pressable>
            <Pressable onPress={() => Vehicles()}
              style={Styles.imageView}>
              <Image style={GlobalImageStyles.homeImage} source={Images.vehicles} />
            </Pressable>
          </ScrollView>
        </View>
        <Modal
          transparent
          animationType='none'
          visible={modalVisible}
          onRequestClose={() => {
            setModalVisible(!modalVisible);
          }}
        >
          <Pressable style={Styles.centeredView}>
            <CustomNavigation changeModal={() => setModalVisible(false) }
                              closeMenu={()=>setModalVisible(false)} />
          </Pressable>
        </Modal>
        <Modal transparent
          visible={Logout}
          onRequestClose={() => {
            setLogout(!Logout);
          }}
        >
          <Pressable style={Styles.centeredView}>
            <LogoutAlert cancel={()=>{setLogout(false)}}
                         logout={()=>{setLogout(false)}}/>
          </Pressable>
        </Modal>
      </ImageBackground>
    </View>
  )
}

export default HomeScreen