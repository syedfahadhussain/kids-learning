import React, { useEffect, useRef } from 'react'
import { Animated, Dimensions, Text, View, ImageBackground,Image} from "react-native";
import { BallIndicator } from 'react-native-indicators';
import Styles from './Styles';
import Images from '../../res/images/Images';
import Login from '../login/LoginScreen'

export default function SplashScreen() {

  // Animation Values....
  const startAnimation = useRef(new Animated.Value(0)).current;

  // Scaling Down Both logo and Title...
  const scaleLogo = useRef(new Animated.Value(1)).current;

  // Offset Animation....
  const moveLogo = useRef(new Animated.ValueXY({ x: 0, y: 0 })).current;

  // Animating LoginScreen Content...
  const contentTransition = useRef(new Animated.Value(Dimensions.get('window').height)).current;

  // Animation Done....
  useEffect(() => {

    // Starting Animation after 500ms....
    setTimeout(() => {

      // Parallel Animation...
      Animated.parallel([
        Animated.timing(
          startAnimation,
          {
            // For same Height for non safe Area Devices...
            toValue: -Dimensions.get('window').height,
            useNativeDriver: true
          }
        ),
        // Animated.timing(
        //   scaleLogo,
        //   {
        //     // Scaling to 0.35
        //     toValue: 0.8,
        //     useNativeDriver: true
        //   }
        // ),
        // Animated.timing(
        //   moveLogo,
        //   {
        //     // Moving to Right Most...
        //     toValue: {
        //       x: 0,
        //       y: (Dimensions.get('window').height) - 155
        //     },
        //     useNativeDriver: true
        //   }
        // ),
        Animated.timing(
          contentTransition,
          {
            toValue: 0,
            useNativeDriver: true
          }
        )
      ])
        .start();
    }, 2500);
  }, [])

  // Going to Move Up like Nav Bar...
  return (

    <View style={Styles.View}>
      <Animated.View style={[Styles.fadingContainer, {
        transform: [
          { translateY: startAnimation }
        ]
      }]}>
        <ImageBackground style={Styles.View}
          source={Images.splashBackground}>
          {/* <Animated. */}
          <View style={Styles.animatedText}>
            {/* <Animated. */}
            <Image source={Images.splashText} 
            // style={{
            //   transform: [
            //     { translateX: moveLogo.x },
            //     { translateY: moveLogo.y },
            //     { scale: scaleLogo },
            //   ]
            // }}
            ></Image>
            {/* Animated. */}
          </View>
          <View style={Styles.loaderView}>
            <BallIndicator style={Styles.loader} color="#FFFFFF"
              count={7} size={45} />
            <Text style={Styles.Text}>
              Version 0.0.1
            </Text>
          </View>
        </ImageBackground>
      </Animated.View>
      <Animated.View style={[Styles.loginView,
      {
        transform: [
          { translateY: contentTransition }
        ]
      }
      ]}>
        <Login />
      </Animated.View>
    </View>

  );
}
