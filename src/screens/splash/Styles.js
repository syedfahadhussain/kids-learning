import Colors from "../../res/colors/Colors"

const Styles = ({
  View: {
    flex: 1,
  },
  imageView: {
    flex: 0.65,
    zIndex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  loaderView: {
    flex: 0.35,
    justifyContent: 'space-between',
  },
  loader: {
    marginTop: 70,
  },
  Text: {
    color: Colors.White,
    flex: 0.3,
    alignSelf: 'center',
  },
  animatedText: {
    flex: 0.7,
    alignItems: 'center',
    justifyContent: 'center',
  },
  loginView: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
  },
  fadingContainer: {
    flex: 1,
    zIndex: 1,
  }
})

export default Styles