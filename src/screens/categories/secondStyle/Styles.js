import Colors from "../../../res/colors/Colors"
import { Platform } from "react-native"

export default Styles = ({
    container: {
        flex: 1,
        backgroundColor: Colors.Transparent,
    },
    headerView: {
        // flex: 0.1,
        ...Platform.select({
            ios: {
                flex: 0.1,
                // backgroundColor:'#000',
                marginTop:'6%'
            },
            android: {
                flex: 0.1,
            },
        }),
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    menuView: {
        flex: 0.2,
        padding: 10,
        zIndex: 1,
    },
    goBackView: {
        flex: 0.2,
        alignItems: 'flex-end',
        padding: 10,
    },
    textView: {
        flex: 0.2,
        marginHorizontal: '12%',
        justifyContent: 'center',
    },
    categoryText: {
        color: Colors.Black,
        fontSize: 24,
        textAlign: 'center',
        fontFamily: 'Poppins-SemiBold',
    },
    contentView: {
        flex: 0.9,
        justifyContent: 'flex-end',
        top: '15%'
    },

    contentCategoryImage: {
        alignItems: 'center',
        // marginTop: '3%',
        marginBottom:'1%',
        // justifyContent: 'flex-end',
        // marginTop: '10%'
    },
    contentCategoryImage2: {
        alignItems: 'center',
        marginBottom: '3%'
    },
    cardView: {
        flex: 1,
        alignItems: 'center',
        // backgroundColor: '#223',
        justifyContent: 'center',
    },
    view: {
        flex: 1,
        // bottom:'6%',
        // backgroundColor: '#888',
        alignItems: 'center',
        justifyContent: 'center',
    },
    bottomView: {
        flex: 0.1
    }
})