import Colors from "../../../res/colors/Colors"
import { Platform } from "react-native"

export default Styles = ({
    container: {
        flex: 1,
        backgroundColor: Colors.Transparent,
    },
    headerView: {
        // flex: 0.2,
        ...Platform.select({
            ios: {
                flex: 0.2,
                // backgroundColor:'#000',
                marginTop:'6%'
            },
            android: {
                flex: 0.2,
            },
        }),
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    menuView: {
        flex: 0.2,
        padding: 10,
        zIndex: 1,
    },
    goBackView: {
        flex: 0.2,
        alignItems: 'flex-end',
        padding: 10,
    },
    headerImage: {
        position: 'absolute',
    },
    contentView: {
        flex: 1,
    },
    contentBack: {
        flex: 0.9,
        marginHorizontal: '3%',
    },
    textView: {
        flex: 0.3,
        justifyContent: 'flex-end',
        marginHorizontal: '3%',
        marginTop:'3%',
        // backgroundColor:'#000'
    },
    contentText: {
        color: Colors.Content_Text,
        fontSize: 26,
        textAlign: 'center',
        fontFamily: 'Poppins-SemiBold',
    },
    contentCategoryImage: {
        alignItems: 'center',
        marginBottom:'2%'
    },
    contentCategoryImage2: {
        alignItems: 'center',
        marginBottom: '3%'
    },
    cardView: {
        flex: 0.7,
        alignItems: 'center',
        justifyContent: 'center',
    }
})