import { Image } from 'react-native'
import React from 'react'
import CategoryComponent from '../../../../global/categoryComponent/CategoryComponent';
import GlobalImageStyles from '../../../../global/Styles/GlobalImageStyles';
import Images from '../../../../res/images/Images';

const AnimalsCategory = () => {
  return (
    <CategoryComponent title='Animals'
      image={<Image style={GlobalImageStyles.smallImage} source={Images.cow} />}
      image2={<Image style={GlobalImageStyles.bigImage} source={Images.dog} />}
      image3={<Image style={GlobalImageStyles.bigImage} source={Images.lion} />}
      image4={<Image style={GlobalImageStyles.smallImage} source={Images.elephant} />}
      image5={<Image style={GlobalImageStyles.smallImage} source={Images.bear} />}
      image6={<Image style={GlobalImageStyles.bigImage} source={Images.bull} />}
      image7={<Image style={GlobalImageStyles.bigImage} source={Images.deer} />}
      image8={<Image style={GlobalImageStyles.smallImage} source={Images.monkey} />}
      image9={<Image style={GlobalImageStyles.bigImage} source={Images.horse} />}
      image10={<Image style={GlobalImageStyles.bigImage} source={Images.tortoise} />}
      first={() => navigation.navigate('CowOption')}
      second={() => navigation.navigate('LionOption')}
      third={() => navigation.navigate('DogOption')}
      fourth={() => navigation.navigate('ElephantOption')}
      fifth={() => navigation.navigate('BearOption')}
      sixth={() => navigation.navigate('DeerOption')}
      seventh={() => navigation.navigate('BullOption')}
      eigth={() => navigation.navigate('MonkeyOption')}
      ninth={() => navigation.navigate('HorseOption')}
      tenth={() => navigation.navigate('TortoiseOption')}
    />
  )
}

export default AnimalsCategory