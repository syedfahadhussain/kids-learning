import React from 'react'
import SelectOption from '../../../../global/selectOption/SelectOption'
import { useNavigation } from '@react-navigation/native'

const BullOption = props => {
  navigation = useNavigation();
  return (
    <SelectOption onclick={() => props.navigation.navigate('BullNumber')}
      onclick2={() => props.navigation.navigate('BullColor')} />
  )
}

export default BullOption