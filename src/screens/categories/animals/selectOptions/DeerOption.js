import React from 'react'
import SelectOption from '../../../../global/selectOption/SelectOption'
import { useNavigation } from '@react-navigation/native'

const DeerOption = props => {
  navigation = useNavigation();
  return (
    <SelectOption onclick={() => props.navigation.navigate('DeerNumber')}
      onclick2={() => props.navigation.navigate('DeerColor')} />
  )
}

export default DeerOption