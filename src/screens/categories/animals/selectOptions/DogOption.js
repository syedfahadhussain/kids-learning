import React from 'react'
import SelectOption from '../../../../global/selectOption/SelectOption'
import { useNavigation } from '@react-navigation/native'

const DogOption = props => {
  navigation = useNavigation();
  return (
    <SelectOption onclick={() => props.navigation.navigate('DogNumber')}
      onclick2={() => props.navigation.navigate('DogColor')} />
  )
}

export default DogOption