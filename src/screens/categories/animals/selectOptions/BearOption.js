import React from 'react'
import SelectOption from '../../../../global/selectOption/SelectOption'
import { useNavigation } from '@react-navigation/native'

const BearOption = props => {
  navigation = useNavigation();
  return (
    <SelectOption onclick={() => props.navigation.navigate('BearNumber')}
      onclick2={() => props.navigation.navigate('BearColor')} />
  )
}

export default BearOption