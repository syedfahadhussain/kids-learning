import React from 'react'
import SelectOption from '../../../../global/selectOption/SelectOption'
import { useNavigation } from '@react-navigation/native'

const TortoiseOption = props => {
  navigation = useNavigation();
  return (
    <SelectOption onclick={() => props.navigation.navigate('TortoiseNumber')}
      onclick2={() => props.navigation.navigate('TortoiseColor')} />
  )
}

export default TortoiseOption