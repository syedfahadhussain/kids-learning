import React from 'react'
import SelectOption from '../../../../global/selectOption/SelectOption'
import { useNavigation } from '@react-navigation/native'

const HorseOption = props => {
  navigation = useNavigation();
  return (
    <SelectOption onclick={() => props.navigation.navigate('HorseNumber')}
      onclick2={() => props.navigation.navigate('HorseColor')} />
  )
}

export default HorseOption