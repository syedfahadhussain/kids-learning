import React from 'react'
import SelectOption from '../../../../global/selectOption/SelectOption'
import { useNavigation } from '@react-navigation/native'

const LionOption = props => {
  navigation = useNavigation();
  return (
    <SelectOption onclick={() => props.navigation.navigate('LionNumber')}
      onclick2={() => props.navigation.navigate('LionColor')} />
  )
}

export default LionOption