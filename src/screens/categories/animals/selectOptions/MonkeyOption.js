import React from 'react'
import SelectOption from '../../../../global/selectOption/SelectOption'
import { useNavigation } from '@react-navigation/native'

const MonkeyOption = props => {
  navigation = useNavigation();
  return (
    <SelectOption onclick={() => props.navigation.navigate('MonkeyNumber')}
      onclick2={() => props.navigation.navigate('MonkeyColor')} />
  )
}

export default MonkeyOption