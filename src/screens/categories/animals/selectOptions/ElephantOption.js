import React from 'react'
import SelectOption from '../../../../global/selectOption/SelectOption'
import { useNavigation } from '@react-navigation/native'

const ElephantOption = props => {
  navigation = useNavigation();
  return (
    <SelectOption onclick={() => props.navigation.navigate('ElephantNumber')}
      onclick2={() => props.navigation.navigate('ElephantColor')} />
  )
}

export default ElephantOption