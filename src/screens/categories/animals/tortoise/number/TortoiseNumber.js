import { View, Text, ImageBackground, Modal, Pressable, Image, FlatList, Alert } from 'react-native'
import React, { useState } from 'react'
import Images from '../../../../../res/images/Images'
import Styles from '../../../secondStyle/Styles';
import { useNavigation } from '@react-navigation/native';
import CustomNavigation from '../../../../../global/topNavigation/CustomNavigation';
import YouWinPopup from '../../../../../global/youWinModal/YouWinPopup';
import KeepTrying from '../../../../../global/keepTryingModal/KeepTrying';
import GlobalImageStyles from '../../../../../global/Styles/GlobalImageStyles';
import MaxArray from '../../../../../global/maxArray/MaxArray';
import FailureAudio from '../../../../../global/sound/FailureAudio';
import SuccessAudio from '../../../../../global/sound/SuccessAudio';


const TortoiseNumber = () => {
  navigation = useNavigation();
  const [arrayNumber, setArrayNumber] = useState(0);
  const [MaxModal, setMaxModal] = useState(false)
  const [MenuModal, setMenuModal] = useState(false);
  const [YouWinModal, setYouWinModal] = useState(false);
  const [KeepgoingModal, setKeepgoingModal] = useState(false);

  const TORTOISE = [
    {
      id: 1,
      item: [
        {
          id: 1,
          image: Images.tortoiseImage,
        },
        {
          id: 2,
          image: Images.tortoiseImage,
        },
        {
          id: 3,
          image: Images.tortoiseImage,
        },
        {
          id: 4,
          image: Images.tortoiseImage,
        },],
      Answer: [
        {
          id: 1,
          answer: false,
          card: 3,
        },
        {
          id: 2,
          answer: true,
          card: 4,
        },
        {
          id: 3,
          answer: false,
          card: 5,
        }
      ]
    },
    {
      id: 2,
      item: [
        {
          id: 1,
          image: Images.tortoiseImage,
        },
        {
          id: 2,
          image: Images.tortoiseImage,
        },
        {
          id: 3,
          image: Images.tortoiseImage,
        }
      ],
      Answer: [
        {
          id: 1,
          answer: true,
          card: 3,
        },
        {
          id: 2,
          answer: false,
          card: 5,
        },
        {
          id: 3,
          answer: false,
          card: 4,
        }
      ]
    },
    {
      id: 3,
      item: [
        {
          id: 1,
          image: Images.tortoiseImage,
        },
        {
          id: 2,
          image: Images.tortoiseImage,
        },
        {
          id: 3,
          image: Images.tortoiseImage,
        },
        {
          id: 4,
          image: Images.tortoiseImage,
        },
        {
          id: 5,
          image: Images.tortoiseImage,
        },
        {
          id: 6,
          image: Images.tortoiseImage,
        }],
      Answer: [
        {
          id: 1,
          answer: false,
          card: 5,
        },
        {
          id: 2,
          answer: true,
          card: 6,
        },
        {
          id: 3,
          answer: false,
          card: 3,
        }
      ]
    },
    {
      id: 4,
      item: [
        {
          id: 1,
          image: Images.tortoiseImage,
        },
        {
          id: 2,
          image: Images.tortoiseImage,
        },
        {
          id: 3,
          image: Images.tortoiseImage,
        },
        {
          id: 4,
          image: Images.tortoiseImage,
        },
        {
          id: 5,
          image: Images.tortoiseImage,
        }],
      Answer: [
        {
          id: 1,
          answer: true,
          card: 5,
        },
        {
          id: 2,
          answer: false,
          card: 6,
        },
        {
          id: 3,
          answer: false,
          card: 3,
        }
      ]
    },
  ];

  const length = () => {
    if (arrayNumber == 3) {
      setMaxModal(true)
    } else {
      setArrayNumber(arrayNumber + 1)
      console.log('arrayNumber', arrayNumber)
    }
  }
  const renderTortoise = ({ item, image }) => (
    <View style={Styles.contentCategoryImage}>
      <Image style={GlobalImageStyles.numberImage} source={item.image} />
    </View>
  );

  const renderCard = ({ item, Answer }) => (
    console.log('====>', item), console.log('====>', Answer),
    <Pressable onPress={() => {
      if (item.answer === true && item.card === 5) {
        [setMaxModal(true), SuccessAudio()]
      }
      else if (item.answer) {
        [setYouWinModal(true), SuccessAudio()]
      } else {
        [setKeepgoingModal(true), FailureAudio()]
      }
    }} style={Styles.contentCategoryImage2}>
      <ImageBackground style={[GlobalImageStyles.cardImage, Styles.cardView]} source={Images.cardImage}>
        <Text style={{ color: '#000', fontSize: 30, fontFamily: 'Poppins-Bold' }}>{item.card}</Text>
      </ImageBackground>
    </Pressable>
  );

  return (
    <View style={Styles.container}>
      <ImageBackground style={Styles.container} source={Images.animalsBackground}>
        <View style={Styles.headerView}>
          <Pressable onPress={() => setMenuModal(true)} style={Styles.menuView}>
            <Image style={GlobalImageStyles.headerIcon} source={Images.menuIcon} />
          </Pressable>
          <Pressable style={Styles.goBackView} onPress={() => { navigation.goBack('null') }}><Image style={GlobalImageStyles.headerIcon} source={Images.goBack} /></Pressable>
        </View>
        <View style={Styles.textView}>
          <Text style={Styles.categoryText}> How many Tortoises are there?</Text>
        </View>
        <View style={Styles.contentView}>
          <View style={Styles.contentCategoryImage} >
            <FlatList
              data={TORTOISE[arrayNumber]?.item}
              scrollEnabled={false}
              renderItem={renderTortoise}
              numColumns={3}
              contentContainerStyle={{ alignItems: 'center' }} />
          </View>
          <View style={Styles.cardView}>
            <FlatList
              data={TORTOISE[arrayNumber]?.Answer}
              scrollEnabled={false}
              renderItem={renderCard} />
          </View>

          <Modal
            transparent
            animationType='none'
            visible={MenuModal}
            onRequestClose={() => {
              setMenuModal(!MenuModal);
            }}
          >
            <Pressable style={Styles.container} >
              <CustomNavigation changeModal={() => { setMenuModal(false) }}
                closeMenu={() => setMenuModal(false)} />
            </Pressable>
          </Modal>

          <Modal transparent
            visible={YouWinModal}
            onRequestClose={() => {
              setYouWinModal(!YouWinModal);
            }}
          >
            <Pressable style={Styles.container} >
              <YouWinPopup changeArrayValue={() => { [length(), setYouWinModal(false), console.log('===>', arrayNumber)] }}
                closeModal={() => { setYouWinModal(false) }} />
            </Pressable>
          </Modal>

          <Modal transparent
            visible={KeepgoingModal}
            onRequestClose={() => {
              setKeepgoingModal(!KeepgoingModal);
            }}
          >
            <Pressable style={Styles.container} >
              <KeepTrying changeArrayValue={() => { [length(), setKeepgoingModal(false), console.log('===>', arrayNumber)] }}
                closeModal={() => { setKeepgoingModal(false) }} />
            </Pressable>
          </Modal>
          <Modal transparent
            visible={MaxModal}
            onRequestClose={() => {
              setMaxModal(!MaxModal);
            }}
          >
            <Pressable style={Styles.container}>
              <MaxArray category={() => { navigation.replace('AnimalsCategory') }} />
            </Pressable>
          </Modal>
          <View style={Styles.bottomView} />
        </View>
      </ImageBackground>
    </View>
  )
}

export default TortoiseNumber