import React from 'react'
import SelectOption from '../../../../global/selectOption/SelectOption'
import { useNavigation } from '@react-navigation/native'

const MangoOption = props => {
  navigation = useNavigation();
  return (
    <SelectOption onclick={() => props.navigation.navigate('MangoNumber')}
      onclick2={() => props.navigation.navigate('MangoColor')} />
  )
}

export default MangoOption