import React from 'react'
import SelectOption from '../../../../global/selectOption/SelectOption'
import { useNavigation } from '@react-navigation/native'

const PineappleOption = props => {
  navigation = useNavigation();
  return (
    <SelectOption onclick={() => props.navigation.navigate('PineappleNumber')}
      onclick2={() => props.navigation.navigate('PineappleColor')} />
  )
}

export default PineappleOption