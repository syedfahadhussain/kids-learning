import React from 'react'
import SelectOption from '../../../../global/selectOption/SelectOption'
import { useNavigation } from '@react-navigation/native'

const AppleOption = props => {
  navigation = useNavigation();
  return (
    <SelectOption onclick={() => props.navigation.navigate('AppleNumber')}
      onclick2={() => props.navigation.navigate('AppleColor')} />
  )
}

export default AppleOption