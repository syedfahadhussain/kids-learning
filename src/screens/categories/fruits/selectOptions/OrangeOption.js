import React from 'react'
import SelectOption from '../../../../global/selectOption/SelectOption'
import { useNavigation } from '@react-navigation/native'

const OrangeOption = props => {
  navigation = useNavigation();
  return (
    <SelectOption onclick={() => props.navigation.navigate('OrangeNumber')}
      onclick2={() => props.navigation.navigate('OrangeColor')} />
  )
}

export default OrangeOption