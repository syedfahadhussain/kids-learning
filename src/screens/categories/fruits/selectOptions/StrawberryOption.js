import React from 'react'
import SelectOption from '../../../../global/selectOption/SelectOption'
import { useNavigation } from '@react-navigation/native'

const StrawberryOption = props => {
  navigation = useNavigation();
  return (
    <SelectOption onclick={() => props.navigation.navigate('StrawberryNumber')}
      onclick2={() => props.navigation.navigate('StrawberryColor')} />
  )
}

export default StrawberryOption