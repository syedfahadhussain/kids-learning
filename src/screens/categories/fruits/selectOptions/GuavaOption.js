import React from 'react'
import SelectOption from '../../../../global/selectOption/SelectOption'
import { useNavigation } from '@react-navigation/native'

const GuavaOption = props => {
  navigation = useNavigation();
  return (
    <SelectOption onclick={() => props.navigation.navigate('GuavaNumber')}
      onclick2={() => props.navigation.navigate('GuavaColor')} />
  )
}

export default GuavaOption