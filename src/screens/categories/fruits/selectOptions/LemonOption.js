import React from 'react'
import SelectOption from '../../../../global/selectOption/SelectOption'
import { useNavigation } from '@react-navigation/native'

const LemonOption = props => {
  navigation = useNavigation();
  return (
    <SelectOption onclick={() => props.navigation.navigate('LemonNumber')}
      onclick2={() => props.navigation.navigate('LemonColor')} />
  )
}

export default LemonOption