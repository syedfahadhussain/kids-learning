import React from 'react'
import SelectOption from '../../../../global/selectOption/SelectOption'
import { useNavigation } from '@react-navigation/native'

const GrapeOption = props => {
  navigation = useNavigation();
  return (
    <SelectOption onclick={() => props.navigation.navigate('GrapeNumber')}
      onclick2={() => props.navigation.navigate('GrapeColor')} />
  )
}

export default GrapeOption