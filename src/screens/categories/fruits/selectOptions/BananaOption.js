import React from 'react'
import SelectOption from '../../../../global/selectOption/SelectOption'
import { useNavigation } from '@react-navigation/native'

const BananaOption = props => {
  navigation = useNavigation();
  return (
    <SelectOption onclick={() => props.navigation.navigate('BananaNumber')}
      onclick2={() => props.navigation.navigate('BananaColor')} />
  )
}

export default BananaOption