import React from 'react'
import CategoryComponent from '../../../../global/categoryComponent/CategoryComponent'
import { Image } from 'react-native'
import GlobalImageStyles from '../../../../global/Styles/GlobalImageStyles';
import Images from '../../../../res/images/Images';

const FruitsCategory = props => {
  return (
    <CategoryComponent title='Fruits'
      image={<Image style={GlobalImageStyles.smallImage} source={Images.apple} />}
      image2={<Image style={GlobalImageStyles.bigImage} source={Images.mango} />}
      image3={<Image style={GlobalImageStyles.bigImage} source={Images.banana} />}
      image4={<Image style={GlobalImageStyles.smallImage} source={Images.orange} />}
      image5={<Image style={GlobalImageStyles.smallImage} source={Images.strawberry} />}
      image6={<Image style={GlobalImageStyles.bigImage} source={Images.watermelon} />}
      image7={<Image style={GlobalImageStyles.bigImage} source={Images.guava} />}
      image8={<Image style={GlobalImageStyles.smallImage} source={Images.grapes} />}
      image9={<Image style={GlobalImageStyles.bigImage} source={Images.pineapple} />}
      image10={<Image style={GlobalImageStyles.bigImage} source={Images.lemon} />}

      first={() => navigation.navigate('AppleOption')}
      second={() => navigation.navigate('BananaOption')}
      third={() => navigation.navigate('MangoOption')}
      fourth={() => navigation.navigate('OrangeOption')}
      fifth={() => navigation.navigate('StrawberryOption')}
      sixth={() => navigation.navigate('GuavaOption')}
      seventh={() => navigation.navigate('WatermelonOption')}
      eigth={() => navigation.navigate('GrapeOption')}
      ninth={() => navigation.navigate('PineappleOption')}
      tenth={() => navigation.navigate('LemonOption')}
    />
  )
}

export default FruitsCategory