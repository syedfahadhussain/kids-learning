import { View, Text, ImageBackground, Modal, Pressable, Image, FlatList, Alert } from 'react-native'
import React, { useState } from 'react'
import Images from '../../../../../res/images/Images'
import Styles from '../../../firstStyle/Styles';
import { useNavigation } from '@react-navigation/native';
import CustomNavigation from '../../../../../global/topNavigation/CustomNavigation';
import YouWinPopup from '../../../../../global/youWinModal/YouWinPopup';
import KeepTrying from '../../../../../global/keepTryingModal/KeepTrying';
import GlobalImageStyles from '../../../../../global/Styles/GlobalImageStyles';
import MaxArray from '../../../../../global/maxArray/MaxArray';
import SuccessAudio from '../../../../../global/sound/SuccessAudio';
import FailureAudio from '../../../../../global/sound/FailureAudio';

const AppleNumber = props => {
  navigation = useNavigation();
  const [arrayNumber, setArrayNumber] = useState(0);
  const [MenuModal, setMenuModal] = useState(false);
  const [YouWinModal, setYouWinModal] = useState(false);
  const [KeepgoingModal, setKeepgoingModal] = useState(false);
  const [MaxModal, setMaxModal] = useState(false)
  const APPLE = [
    {
      id: 1,
      item: [
        {
          id: 1,
          image: Images.appleImage,
        },
        {
          id: 2,
          image: Images.appleImage,
        },
        {
          id: 3,
          image: Images.appleImage,
        }],
      Answer: [
        {
          id: 1,
          answer: true,
          card: 3,
        },
        {
          id: 2,
          answer: false,
          card: 4,
        },
        {
          id: 3,
          answer: false,
          card: 5,
        }
      ]
    },
    {
      id: 2,
      item: [
        {
          id: 1,
          image: Images.appleImage,
        },
        {
          id: 2,
          image: Images.appleImage,
        },
        {
          id: 3,
          image: Images.appleImage,
        },
        {
          id: 4,
          image: Images.appleImage,
        }],
      Answer: [
        {
          id: 1,
          answer: false,
          card: 3,
        },
        {
          id: 2,
          answer: false,
          card: 5,
        },
        {
          id: 3,
          answer: true,
          card: 4,
        }
      ]
    },
    {
      id: 3,
      item: [
        {
          id: 1,
          image: Images.appleImage,
        },
        {
          id: 2,
          image: Images.appleImage,
        },
        {
          id: 3,
          image: Images.appleImage,
        },
        {
          id: 4,
          image: Images.appleImage,
        },
        {
          id: 5,
          image: Images.appleImage,
        }],
      Answer: [
        {
          id: 1,
          answer: true,
          card: 5,
        },
        {
          id: 2,
          answer: false,
          card: 4,
        },
        {
          id: 3,
          answer: false,
          card: 3,
        }
      ]
    },
    {
      id: 4,
      item: [
        {
          id: 1,
          image: Images.appleImage,
        },
        {
          id: 2,
          image: Images.appleImage,
        },
        {
          id: 3,
          image: Images.appleImage,
        },
        {
          id: 4,
          image: Images.appleImage,
        },
        {
          id: 5,
          image: Images.appleImage,
        },
        {
          id: 6,
          image: Images.appleImage,
        }],
      Answer: [
        {
          id: 1,
          answer: false,
          card: 5,
        },
        {
          id: 2,
          answer: true,
          card: 6,
        },
        {
          id: 3,
          answer: false,
          card: 3,
        }
      ]
    },
  ]

  const length = () => {
    if (arrayNumber == 3) {
      setMaxModal(true)
    } else {
      setArrayNumber(arrayNumber + 1)
      console.log('arrayNumber', arrayNumber)
    }
  }

  const Item = ({ image }) => (
    <View style={Styles.contentCategoryImage}>
      <Image style={GlobalImageStyles.appleNumberImage} source={image} />
    </View>
  );

  const renderApple = ({ item }) => (
    console.log('====><', item),
    <Item name={item.name}
      image={item.image} />
  );

  const renderCard = ({ item, Answer }) => (
    console.log('====>', item),
    <Pressable onPress={() => {
      if (item.answer === true && item.card === 6) {
        [setMaxModal(true), SuccessAudio()]
      }
      else if (item.answer) {
        [setYouWinModal(true), SuccessAudio()]
      } else {
        [setKeepgoingModal(true), FailureAudio()]
      }
    }} style={Styles.contentCategoryImage2}>
      <ImageBackground style={[GlobalImageStyles.cardImage, Styles.cardView]} source={Images.cardImage}>
        <Text style={{ color: '#000', fontSize: 30, fontFamily: 'Poppins-Bold' }}>{item.card}</Text>
      </ImageBackground>
    </Pressable>
  );

  return (
    <View style={Styles.container}>
      <ImageBackground resizeMode='stretch' style={Styles.container} source={Images.categoryBackground}>
        <View style={Styles.headerView}>
          <Pressable onPress={() => setMenuModal(true)} style={Styles.menuView}>
            <Image style={GlobalImageStyles.headerIcon} source={Images.menuIcon} />
          </Pressable>
          <View style={Styles.headerImage}>
            <Image style={GlobalImageStyles.treeBranch} source={Images.appleBranch}></Image>
          </View>
          <Pressable style={Styles.goBackView} onPress={() => { navigation.goBack('null') }}><Image style={GlobalImageStyles.headerIcon} source={Images.goBack} /></Pressable>
        </View>
        <View style={Styles.contentView}>
          <ImageBackground style={Styles.contentBack} resizeMode='stretch' source={Images.categoryContent}>
            <View style={Styles.textView}>
              <Text style={Styles.contentText}> How many Apples are there?</Text>
            </View>
            <View style={Styles.contentCategoryImage} >
              <FlatList
                data={APPLE[arrayNumber]?.item}
                scrollEnabled={false}
                numColumns={3}
                contentContainerStyle={{ alignItems: 'center', }}
                renderItem={renderApple} />
            </View>
            <View style={Styles.cardView}>
              <FlatList
                data={APPLE[arrayNumber]?.Answer}
                scrollEnabled={false}
                renderItem={renderCard} />
            </View>

            <Modal
              transparent
              animationType='none'
              visible={MenuModal}
              onRequestClose={() => {
                setMenuModal(!MenuModal);
              }}
            >
              <Pressable style={Styles.container} >
                <CustomNavigation changeModal={() => { setMenuModal(false) }}
                  closeMenu={() => setMenuModal(false)} />
              </Pressable>
            </Modal>

            <Modal transparent
              visible={YouWinModal}
              onRequestClose={() => {
                setYouWinModal(!YouWinModal);
              }}
            >
              <Pressable style={Styles.container} >
                <YouWinPopup
                  changeArrayValue={() => { [length(), setYouWinModal(false), console.log('===>', arrayNumber)] }}
                  closeModal={() => { setYouWinModal(false) }} />
              </Pressable>
            </Modal>

            <Modal transparent
              visible={KeepgoingModal}
              onRequestClose={() => {
                setKeepgoingModal(!KeepgoingModal);
              }}
            >
              <Pressable style={Styles.container} >
                <KeepTrying
                  changeArrayValue={() => { [length(), setKeepgoingModal(false), console.log('===>', arrayNumber)] }}
                  closeModal={() => { setKeepgoingModal(false) }} />
              </Pressable>
            </Modal>
            <Modal transparent
              visible={MaxModal}
              onRequestClose={() => {
                setMaxModal(!MaxModal);
              }}
            >
              <Pressable style={Styles.container}>
                <MaxArray category={() => { navigation.replace('FruitsCategory') }} />
              </Pressable>
            </Modal>
          </ImageBackground>
        </View>
      </ImageBackground>
    </View>
  )
}

export default AppleNumber