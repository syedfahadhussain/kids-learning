import { View, Text, ImageBackground, Modal, Pressable, Image, ScrollView, FlatList } from 'react-native'
import React, { useState } from 'react'
import Images from '../../../../../res/images/Images'
import Styles from '../../../firstStyle/Styles';
import { useNavigation } from '@react-navigation/native';
import CustomNavigation from '../../../../../global/topNavigation/CustomNavigation';
import YouWinPopup from '../../../../../global/youWinModal/YouWinPopup';
import KeepTrying from '../../../../../global/keepTryingModal/KeepTrying';
import GlobalImageStyles from '../../../../../global/Styles/GlobalImageStyles';
import SuccessAudio from '../../../../../global/sound/SuccessAudio';
import FailureAudio from '../../../../../global/sound/FailureAudio';

const GrapesColor = () => {
  navigation = useNavigation();
  const [arrayNumber, setArrayNumber] = useState(0);
  const [MenuModal, setMenuModal] = useState(false);
  const [YouWinModal, setYouWinModal] = useState(false);
  const [KeepgoingModal, setKeepgoingModal] = useState(false);

  const GRAPE = [
    {
      id: 1,
      item: [
        {
          id: 1, image: Images.grapeImage
        },
      ],
      Answer: [
        {
          id: 1,
          answer: true,
          card: 'Purple',
        },
        {
          id: 2,
          answer: false,
          card: 'Red',
        },
        {
          id: 3,
          answer: false,
          card: 'Green',
        }
      ]
    }
  ]

  const length = () => {
    navigation.navigate('PineappleColor')
  }

  const Item = ({ image }) => (
    <View style={Styles.contentCategoryImage}>
      <Image style={GlobalImageStyles.colorImage} source={image} />
    </View>
  );

  const renderGrape = ({ item }) => (
    console.log('====><', item),
    <Item name={item.name}
      image={item.image} />
  );

  const renderCard = ({ item, Answer }) => (
    console.log('====>', item), console.log('====>', Answer),
    <Pressable onPress={() => {
      if (item.answer) {
        [setYouWinModal(true), SuccessAudio()]
      } else {
        [setKeepgoingModal(true), FailureAudio()]
      }
    }} style={Styles.contentCategoryImage2}>
      <ImageBackground style={[GlobalImageStyles.cardImage, Styles.cardView]} source={Images.cardImage}>
        <Text style={{ color: '#000', fontSize: 30, fontFamily: 'Poppins-Bold' }}>{item.card}</Text>
      </ImageBackground>
    </Pressable>
  );

  return (
    <View style={Styles.container}>
      <ImageBackground resizeMode='stretch' style={Styles.container} source={Images.categoryBackground}>
        <View style={Styles.headerView}>
          <Pressable onPress={() => setMenuModal(true)} style={Styles.menuView}>
            <Image style={GlobalImageStyles.headerIcon} source={Images.menuIcon} />
          </Pressable>
          <View style={Styles.headerImage}>
            <Image style={GlobalImageStyles.treeBranch} source={Images.grapeBranch}></Image>
          </View>
          <Pressable style={Styles.goBackView} onPress={() => { navigation.replace('FruitsCategory') }}><Image style={GlobalImageStyles.headerIcon} source={Images.goBack} /></Pressable>
        </View>
        <View style={Styles.contentView}>
          <ImageBackground style={Styles.contentBack} resizeMode='stretch' source={Images.categoryContent}>
            <View style={Styles.textView}>
              <Text style={Styles.contentText}> What is the color of Grape?</Text>
            </View>
            <View style={Styles.contentCategoryImage} >
              <FlatList
                data={GRAPE[arrayNumber]?.item}
                scrollEnabled={false}
                numColumns={3}
                contentContainerStyle={{ alignItems: 'center', }}
                renderItem={renderGrape} />
            </View>
            <View style={Styles.cardView}>
              <FlatList
                data={GRAPE[arrayNumber]?.Answer}
                scrollEnabled={false}
                renderItem={renderCard} />
            </View>

            <Modal
              transparent
              animationType='none'
              visible={MenuModal}
              onRequestClose={() => {
                setMenuModal(!MenuModal);
              }}
            >
              <Pressable style={Styles.container} >
                <CustomNavigation changeModal={() => { setMenuModal(false) }}
                  closeMenu={() => setMenuModal(false)} />
              </Pressable>
            </Modal>

            <Modal transparent
              visible={YouWinModal}
              onRequestClose={() => {
                setYouWinModal(!YouWinModal);
              }}
            >
              <Pressable style={Styles.container} >
                <YouWinPopup changeArrayValue={() => { [length(), setYouWinModal(false), console.log('===>', arrayNumber)] }}
                  closeModal={() => { setYouWinModal(false) }} />
              </Pressable>
            </Modal>

            <Modal transparent
              visible={KeepgoingModal}
              onRequestClose={() => {
                setKeepgoingModal(!KeepgoingModal);
              }}
            >
              <Pressable style={Styles.container} >
                <KeepTrying changeArrayValue={() => { [length(), setKeepgoingModal(false), console.log('===>', arrayNumber)] }}
                  closeModal={() => { setKeepgoingModal(false) }} />
              </Pressable>
            </Modal>
          </ImageBackground>
        </View>
      </ImageBackground>
    </View>
  )
}

export default GrapesColor