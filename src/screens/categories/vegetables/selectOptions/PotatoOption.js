import React from 'react'
import SelectOption from '../../../../global/selectOption/SelectOption'
import { useNavigation } from '@react-navigation/native'

const PotatoOption = props => {
  navigation = useNavigation();
  return (
    <SelectOption onclick={() => props.navigation.navigate('PotatoNumber')}
      onclick2={() => props.navigation.navigate('PotatoColor')} />
  )
}

export default PotatoOption