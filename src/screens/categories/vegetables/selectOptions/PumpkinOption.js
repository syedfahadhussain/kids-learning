import React from 'react'
import SelectOption from '../../../../global/selectOption/SelectOption'
import { useNavigation } from '@react-navigation/native'

const PumpkinOption = props => {
  navigation = useNavigation();
  return (
    <SelectOption onclick={() => props.navigation.navigate('PumpkinNumber')}
      onclick2={() => props.navigation.navigate('PumpkinColor')} />
  )
}

export default PumpkinOption