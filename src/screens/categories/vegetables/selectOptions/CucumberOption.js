import React from 'react'
import SelectOption from '../../../../global/selectOption/SelectOption'
import { useNavigation } from '@react-navigation/native'

const CucumberOption = props => {
  navigation = useNavigation();
  return (
    <SelectOption onclick={() => props.navigation.navigate('CucumberNumber')}
      onclick2={() => props.navigation.navigate('CucumberColor')} />
  )
}

export default CucumberOption