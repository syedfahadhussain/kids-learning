import React from 'react'
import SelectOption from '../../../../global/selectOption/SelectOption'
import { useNavigation } from '@react-navigation/native'

const ChilliOption = props => {
  navigation = useNavigation();
  return (
    <SelectOption onclick={() => props.navigation.navigate('ChilliNumber')}
      onclick2={() => props.navigation.navigate('ChilliColor')} />
  )
}

export default ChilliOption