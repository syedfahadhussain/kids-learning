import React from 'react'
import SelectOption from '../../../../global/selectOption/SelectOption'
import { useNavigation } from '@react-navigation/native'

const CauliflowerOption = props => {
  navigation = useNavigation();
  return (
    <SelectOption onclick={() => props.navigation.navigate('CauliflowerNumber')}
      onclick2={() => props.navigation.navigate('CauliflowerColor')} />
  )
}

export default CauliflowerOption