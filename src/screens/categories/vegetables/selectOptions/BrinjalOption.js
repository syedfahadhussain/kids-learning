import React from 'react'
import SelectOption from '../../../../global/selectOption/SelectOption'
import { useNavigation } from '@react-navigation/native'

const BrinjalOption = props => {
  navigation = useNavigation();
  return (
    <SelectOption onclick={() => props.navigation.navigate('BrinjalNumber')}
      onclick2={() => props.navigation.navigate('BrinjalColor')} />
  )
}

export default BrinjalOption