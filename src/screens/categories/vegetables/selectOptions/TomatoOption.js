import React from 'react'
import SelectOption from '../../../../global/selectOption/SelectOption'
import { useNavigation } from '@react-navigation/native'

const TomatoOption = props => {
  navigation = useNavigation();
  return (
    <SelectOption onclick={() => props.navigation.navigate('TomatoNumber')}
      onclick2={() => props.navigation.navigate('TomatoColor')} />
  )
}

export default TomatoOption