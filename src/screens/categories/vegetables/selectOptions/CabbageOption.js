import React from 'react'
import SelectOption from '../../../../global/selectOption/SelectOption'
import { useNavigation } from '@react-navigation/native'

const CabbageOption = props => {
  navigation = useNavigation();
  return (
    <SelectOption onclick={() => props.navigation.navigate('CabbageNumber')}
      onclick2={() => props.navigation.navigate('CabbageColor')} />
  )
}

export default CabbageOption