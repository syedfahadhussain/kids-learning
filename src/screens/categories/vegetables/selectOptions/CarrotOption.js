import React from 'react'
import SelectOption from '../../../../global/selectOption/SelectOption'
import { useNavigation } from '@react-navigation/native'

const CarrotOption = props => {
  navigation = useNavigation();
  return (
    <SelectOption onclick={() => props.navigation.navigate('CarrotNumber')}
      onclick2={() => props.navigation.navigate('CarrotColor')} />
  )
}

export default CarrotOption