import React from 'react'
import { Image } from 'react-native';
import CategoryComponent from '../../../../global/categoryComponent/CategoryComponent'
import GlobalImageStyles from '../../../../global/Styles/GlobalImageStyles'
import Images from '../../../../res/images/Images'

const VegetablesCategory = props => {
  return (
    <CategoryComponent title='Vegetables'
      image={<Image style={GlobalImageStyles.smallImage} source={Images.tomato} />}
      image2={<Image style={GlobalImageStyles.bigImage} source={Images.brinjal} />}
      image3={<Image style={GlobalImageStyles.bigImage} source={Images.carrot} />}
      image4={<Image style={GlobalImageStyles.smallImage} source={Images.potato} />}
      image5={<Image style={GlobalImageStyles.smallImage} source={Images.chilli} />}
      image6={<Image style={GlobalImageStyles.bigImage} source={Images.cauliflower} />}
      image7={<Image style={GlobalImageStyles.bigImage} source={Images.pumpkin} />}
      image8={<Image style={GlobalImageStyles.smallImage} source={Images.cabbage} />}
      image9={<Image style={GlobalImageStyles.bigImage} source={Images.onion} />}
      image10={<Image style={GlobalImageStyles.bigImage} source={Images.cucumber} />}
      first={() => navigation.navigate('TomatoOption')}
      second={() => navigation.navigate('CarrotOption')}
      third={() => navigation.navigate('BrinjalOption')}
      fourth={() => navigation.navigate('PotatoOption')}
      fifth={() => navigation.navigate('ChilliOption')}
      sixth={() => navigation.navigate('PumpkinOption')}
      seventh={() => navigation.navigate('CauliflowerOption')}
      eigth={() => navigation.navigate('CabbageOption')}
      ninth={() => navigation.navigate('OnionOption')}
      tenth={() => navigation.navigate('CucumberOption')}
    />
  )
}

export default VegetablesCategory