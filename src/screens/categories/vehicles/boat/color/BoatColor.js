import { View, Text, ImageBackground, Modal, Pressable, Image, FlatList } from 'react-native'
import React, { useState } from 'react'
import Images from '../../../../../res/images/Images'
import Styles from '../../../secondStyle/Styles';
import { useNavigation } from '@react-navigation/native';
import CustomNavigation from '../../../../../global/topNavigation/CustomNavigation';
import YouWinPopup from '../../../../../global/youWinModal/YouWinPopup';
import KeepTrying from '../../../../../global/keepTryingModal/KeepTrying';
import GlobalImageStyles from '../../../../../global/Styles/GlobalImageStyles';
import FailureAudio from '../../../../../global/sound/FailureAudio';
import SuccessAudio from '../../../../../global/sound/SuccessAudio';


const BoatColor = () => {
  navigation = useNavigation();
  const [arrayNumber, setArrayNumber] = useState(0)
  const [MenuModal, setMenuModal] = useState(false);
  const [YouWinModal, setYouWinModal] = useState(false);
  const [KeepgoingModal, setKeepgoingModal] = useState(false);

  const length = () => {
    navigation.navigate('AutoColor')
  }

  const BOAT = [
    {
      id: 1,
      item: [
        {
          id: 1, image: Images.boatImage
        },
      ],
      Answer: [
        {
          id: 1,
          answer: true,
          card: 'White',
        },
        {
          id: 2,
          answer: false,
          card: 'Red',
        },
        {
          id: 3,
          answer: false,
          card: 'Yellow',
        }
      ]
    }
  ]

  const Item = ({ image }) => (
    <View style={Styles.contentCategoryImage}>
      <Image style={GlobalImageStyles.colorImage} source={image} />
    </View>
  );

  const renderBoat = ({ item }) => (
    console.log('====><', item),
    <Item name={item.name}
      image={item.image} />
  );

  const renderCard = ({ item, Answer }) => (
    console.log('====>', item), console.log('====>', Answer),
    <Pressable onPress={() => {
      if (item.answer) {
        [setYouWinModal(true), SuccessAudio()]
      } else {
        [setKeepgoingModal(true), FailureAudio()]
      }
    }} style={Styles.contentCategoryImage2}>
      <ImageBackground style={[GlobalImageStyles.cardImage, Styles.cardView]} source={Images.cardImage}>
        <Text style={{ color: '#000', fontSize: 30, fontFamily: 'Poppins-Bold' }}>{item.card}</Text>
      </ImageBackground>
    </Pressable>
  );

  return (
    <View style={Styles.container}>
      <ImageBackground style={Styles.container} source={Images.categoryBackground2}>
        <View style={Styles.headerView}>
          <Pressable onPress={() => setMenuModal(true)} style={Styles.menuView}>
            <Image style={GlobalImageStyles.headerIcon} source={Images.menuIcon} />
          </Pressable>
          <Pressable style={Styles.goBackView} onPress={() => { navigation.replace('VehiclesCategory') }}><Image style={GlobalImageStyles.headerIcon} source={Images.goBack} /></Pressable>
        </View>
        <View style={Styles.textView}>
          <Text style={Styles.categoryText}> What is the color of Boat?</Text>
        </View>
        <View style={Styles.contentView}>
          <View style={Styles.contentCategoryImage} >
            <FlatList
              data={BOAT[arrayNumber].item}
              scrollEnabled={false}
              numColumns={3}
              contentContainerStyle={{ alignItems: 'center', }}
              renderItem={renderBoat} />
          </View>
          <View style={Styles.cardView}>
            <FlatList
              data={BOAT[arrayNumber].Answer}
              scrollEnabled={false}
              renderItem={renderCard} />
          </View>

          <Modal
            transparent
            animationType='none'
            visible={MenuModal}
            onRequestClose={() => {
              setMenuModal(!MenuModal);
            }}
          >
            <Pressable style={Styles.container} >
              <CustomNavigation changeModal={() => { setMenuModal(false) }}
                closeMenu={() => setMenuModal(false)} />
            </Pressable>
          </Modal>

          <Modal transparent
            visible={YouWinModal}
            onRequestClose={() => {
              setYouWinModal(!YouWinModal);
            }}
          >
            <Pressable style={Styles.container} >
              <YouWinPopup changeArrayValue={() => { [length(), setYouWinModal(false)] }}
                closeModal={() => { setYouWinModal(false) }} />
            </Pressable>
          </Modal>

          <Modal transparent
            visible={KeepgoingModal}
            onRequestClose={() => {
              setKeepgoingModal(!KeepgoingModal);
            }}
          >
            <Pressable style={Styles.container} >
              <KeepTrying changeArrayValue={() => { [length(), setKeepgoingModal(false)] }}
                closeModal={() => { setKeepgoingModal(false) }} />
            </Pressable>
          </Modal>
          <View style={Styles.bottomView} />
        </View>
      </ImageBackground>
    </View>
  )
}

export default BoatColor