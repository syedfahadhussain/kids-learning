import React from 'react'
import SelectOption from '../../../../global/selectOption/SelectOption'
import { useNavigation } from '@react-navigation/native'

const TrainOption = props => {
  navigation = useNavigation();
  return (
    <SelectOption onclick={() => props.navigation.navigate('TrainNumber')}
      onclick2={() => props.navigation.navigate('TrainColor')} />
  )
}

export default TrainOption