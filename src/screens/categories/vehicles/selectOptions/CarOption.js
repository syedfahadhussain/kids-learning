import React from 'react'
import SelectOption from '../../../../global/selectOption/SelectOption'
import { useNavigation } from '@react-navigation/native'

const CarOption = props => {
  navigation = useNavigation();
  return (
    <SelectOption onclick={() => props.navigation.navigate('CarNumber')}
      onclick2={() => props.navigation.navigate('CarColor')} />
  )
}

export default CarOption