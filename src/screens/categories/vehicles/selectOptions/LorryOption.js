import React from 'react'
import SelectOption from '../../../../global/selectOption/SelectOption'
import { useNavigation } from '@react-navigation/native'

const LorryOption = props => {
  navigation = useNavigation();
  return (
    <SelectOption onclick={() => props.navigation.navigate('LorryNumber')}
      onclick2={() => props.navigation.navigate('LorryColor')} />
  )
}

export default LorryOption