import React from 'react'
import SelectOption from '../../../../global/selectOption/SelectOption'
import { useNavigation } from '@react-navigation/native'

const AutoOption = props => {
  navigation = useNavigation();
  return (
    <SelectOption onclick={() => props.navigation.navigate('AutoNumber')}
      onclick2={() => props.navigation.navigate('AutoColor')} />
  )
}

export default AutoOption