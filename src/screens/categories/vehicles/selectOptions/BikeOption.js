import React from 'react'
import SelectOption from '../../../../global/selectOption/SelectOption'
import { useNavigation } from '@react-navigation/native'

const BikeOption = props => {
  navigation = useNavigation();
  return (
    <SelectOption onclick={() => props.navigation.navigate('BikeNumber')}
      onclick2={() => props.navigation.navigate('BikeColor')} />
  )
}

export default BikeOption