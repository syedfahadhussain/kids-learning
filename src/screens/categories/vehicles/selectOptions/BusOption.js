import React from 'react'
import SelectOption from '../../../../global/selectOption/SelectOption'
import { useNavigation } from '@react-navigation/native'

const BusOption = props => {
  navigation = useNavigation();
  return (
    <SelectOption onclick={() => props.navigation.navigate('BusNumber')}
      onclick2={() => props.navigation.navigate('BusColor')} />
  )
}

export default BusOption