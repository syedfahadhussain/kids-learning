import React from 'react'
import SelectOption from '../../../../global/selectOption/SelectOption'
import { useNavigation } from '@react-navigation/native'

const VanOption = props => {
  navigation = useNavigation();
  return (
    <SelectOption onclick={() => props.navigation.navigate('VanNumber')}
      onclick2={() => props.navigation.navigate('VanColor')} />
  )
}

export default VanOption