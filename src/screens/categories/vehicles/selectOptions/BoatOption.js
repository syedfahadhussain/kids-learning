import React from 'react'
import SelectOption from '../../../../global/selectOption/SelectOption'
import { useNavigation } from '@react-navigation/native'

const BoatOption = props => {
  navigation = useNavigation();
  return (
    <SelectOption onclick={() => props.navigation.navigate('BoatNumber')}
      onclick2={() => props.navigation.navigate('BoatColor')} />
  )
}

export default BoatOption