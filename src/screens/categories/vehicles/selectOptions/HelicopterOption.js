import React from 'react'
import SelectOption from '../../../../global/selectOption/SelectOption'
import { useNavigation } from '@react-navigation/native'

const HelicopterOption = props => {
  navigation = useNavigation();
  return (
    <SelectOption onclick={() => props.navigation.navigate('HelicopterNumber')}
      onclick2={() => props.navigation.navigate('HelicopterColor')} />
  )
}

export default HelicopterOption