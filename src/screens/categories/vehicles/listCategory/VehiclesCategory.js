import Images from '../../../../res/images/Images'
import React from 'react'
import { Image } from 'react-native'
import CategoryComponent from '../../../../global/categoryComponent/CategoryComponent'
import GlobalImageStyles from '../../../../global/Styles/GlobalImageStyles'

const VehiclesCategory = () => {
  return (
    <CategoryComponent title='Vehicles'
      image={<Image style={GlobalImageStyles.smallImage} source={Images.car} />}
      image2={<Image style={GlobalImageStyles.bigImage} source={Images.lorry} />}
      image3={<Image style={GlobalImageStyles.bigImage} source={Images.train} />}
      image4={<Image style={GlobalImageStyles.smallImage} source={Images.helicopter} />}
      image5={<Image style={GlobalImageStyles.smallImage} source={Images.bike} />}
      image6={<Image style={GlobalImageStyles.bigImage} source={Images.auto} />}
      image7={<Image style={GlobalImageStyles.bigImage} source={Images.boat} />}
      image8={<Image style={GlobalImageStyles.smallImage} source={Images.van} />}
      image9={<Image style={GlobalImageStyles.bigImage} source={Images.bus} />}
      image10={<Image style={GlobalImageStyles.bigImage} source={Images.jcb} />}
      first={() => navigation.navigate('CarOption')}
      second={() => navigation.navigate('TrainOption')}
      third={() => navigation.navigate('LorryOption')}
      fourth={() => navigation.navigate('HelicopterOption')}
      fifth={() => navigation.navigate('BikeOption')}
      sixth={() => navigation.navigate('BoatOption')}
      seventh={() => navigation.navigate('AutoOption')}
      eigth={() => navigation.navigate('VanOption')}
      ninth={() => navigation.navigate('BusOption')}
      tenth={() => navigation.navigate('JcbOption')}

    />
  )
}

export default VehiclesCategory