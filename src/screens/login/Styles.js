import Colors from "../../res/colors/Colors";
import { Dimensions } from 'react-native'
const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;
export default Styles = ({
    Background: {
        flex: 1,
    },
    animatedText: {
        flex: 0.3,
        alignItems: 'center',
        justifyContent: 'center',
    },
    image: {
        resizeMode: 'contain',
        height: Height / 100 * 17,
        width: Width / 100 * 48,
    },
    contentView: {
        flex: 0.7,
        alignItems: 'center',
    },
    textView: {
        flex: 0.25,
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    topText: {
        fontSize: 30,
        color: Colors.Login_Text,
        fontFamily: 'Poppins-Bold',
    },
    bottomText: {
        textAlign: 'center',
        marginHorizontal: '5%',
        color: Colors.Login_Text,
        fontFamily: 'Poppins-Medium',
        fontSize: 13,
    },
    imageView: {
        flex: 0.17,
    },
    imageViewModal: {
        alignSelf: 'center',
        marginTop: '6%',
        flex: 0.25,
        justifyContent: 'center',
    },
    centeredView: {
        flex: 1,
        backgroundColor: Colors.Transparent,
        justifyContent: "center",
    },
    modalBackground: {
        height: Dimensions.get('window').height / 2.5,
        marginHorizontal: '6%',
        justifyContent: 'center',
    },
    modalView: {
        flex: 1,
        justifyContent: 'center',
        marginHorizontal: '12%',
    },
    modalText: {
        fontFamily: 'Poppins-SemiBold',
        fontSize: 18,
        color: Colors.Modal_Text_Color,
    },
    modalInput: {
        marginTop: '5%',
        borderColor: Colors.Modal_Input,
        borderWidth: 2,
        borderRadius: 30,
        height:'15%',
        color: Colors.Modal_Text_Color,
        paddingLeft: 20,
        paddingVertical: 0,
        backgroundColor: Colors.White,
    },
    modalImage: {
        resizeMode: 'contain',
        height: Dimensions.get('window').height / 100 * 11,
        width: Dimensions.get('window').width / 100 * 40,
    }
})

