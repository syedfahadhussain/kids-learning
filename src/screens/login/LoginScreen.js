import { View, Text, ImageBackground, Pressable, Image, Modal, TextInput, Alert, BackHandler, Platform } from 'react-native'
import React, { useState, useEffect } from 'react'
import Styles from './Styles'
import { useNavigation } from '@react-navigation/native'
import Images from '../../res/images/Images'
import Strings from '../../res/strings/Strings'
import AsyncStorage from '@react-native-async-storage/async-storage'
import GlobalImageStyles from '../../global/Styles/GlobalImageStyles'
import LoginAlert from '../../components/loginAlert/LoginAlert'
import crashlytics from '@react-native-firebase/crashlytics';

const LoginScreen = () => {
  const [modalVisible, setModalVisible] = useState(false);
  const [name, setName] = useState('');
  const [alertModal, setAlertModal] = useState(false)
  navigation = useNavigation();
  // const route = params
  useEffect(() => {
    setData;
    const backAction = () => {
      BackHandler.exitApp()
      return true;
    }

    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backAction,
    );

    return () => backHandler.remove();
  }, []);
  const Home = () => {
    navigation.navigate('HomeScreen')
    // ,{setModalVisible()}
  }

  const setData = async () => {
    if (name.length === 0) {
      if(Platform.OS==='ios'){
        // setAlertModal(true)
        console.log('dssa', name)
       Alert.alert('Please Enter Your Nick Name')
        console.log('check', name.length)
      }else{
        console.log('check', name.length)
        // setTimeout(() => {
          setAlertModal(true)
      // }, 300);
      }   
    } else {
      try {
        await AsyncStorage.setItem('UserName', name);
        Home(setModalVisible(false));
      } catch (error) {
        console.log(error);
      }
    }
  }

  return (
    <View style={Styles.Background}>
      <ImageBackground style={Styles.Background} source={Images.loginBackground}>
        <View style={Styles.animatedText}>
            <Image source={Images.splashText} style={Styles.image}
            ></Image>
          </View>
          <View style={{flex:0.05,}}/>
        <View style={Styles.contentView}>
          <View style={Styles.textView}>
            <Text style={Styles.topText}>
              {Strings.welcome}
            </Text>
            <Text style={Styles.bottomText}>
              {Strings.loginText}
            </Text>
          </View>
          <Pressable style={Styles.imageView}
            onPress={() => setModalVisible(true)}>
            <Image style={GlobalImageStyles.loginImage} source={Images.guestLogin} />
          </Pressable>
        </View>
        <Modal
          animationType="fade"
          transparent
          visible={modalVisible}
          onRequestClose={() => {
            setModalVisible(!modalVisible);
          }}
        >
          <Pressable style={Styles.centeredView}>
            <ImageBackground style={Styles.modalBackground} resizeMode='stretch' source={Images.loginPopup}>
              <View style={Styles.modalView}>
                <Text style={Styles.modalText}>
                  {Strings.modalText}
                </Text>

                <TextInput style={Styles.modalInput}
                  onChangeText={(value) => setName(value.trim())}
                  autoCapitalize='words'
                  maxLength={10}
                  placeholderTextColor="#BEBEBE"
                  placeholder='Enter your Nick Name' />
                <Pressable style={Styles.imageViewModal}
                  onPress={() => setData()}>
                  <Image style={Styles.modalImage}
                    source={Images.submitButton} />
                </Pressable>
              </View>
            </ImageBackground>
          </Pressable>
        </Modal>
        <Modal transparent
          visible={alertModal}
          onRequestClose={() => {
            setAlertModal(!alertModal);
          }}
        >
          <Pressable style={Styles.centeredView}>
            <LoginAlert close={() => { setAlertModal(false) }} />
          </Pressable>
        </Modal>
      </ImageBackground>
    </View>
  )
}

export default LoginScreen