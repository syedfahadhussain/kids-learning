import { View, Text, ImageBackground, Pressable, Image, Modal, ScrollView, BackHandler , Linking} from 'react-native'
import React, { useState, useEffect } from 'react'
import Images from '../../../res/images/Images'
import Styles from './Styles'
import CustomNavigation from '../../../global/topNavigation/CustomNavigation'
import { useNavigation } from '@react-navigation/native';

const PrivacyPolicy = () => {
  navigation = useNavigation();

  useEffect(() => {
    const backAction = () => {
      navigation.replace('HomeScreen');
      return true;
    }

    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backAction,
    );

    return () => backHandler.remove();
  }, []);

  const [modalVisible, setModalVisible] = useState(false);
  return (
    <View style={Styles.container}>
      <ImageBackground style={Styles.container} source={Images.privacyBack}>
        <View style={Styles.headerView}>
          <Pressable onPress={() => setModalVisible(true)}>
            <Image style={GlobalImageStyles.headerIcon} source={Images.menuIcon} />
          </Pressable>
          <Text style={Styles.headerText}>Privacy Policy</Text>
          <Pressable onPress={() => { navigation.replace('HomeScreen') }}><Image style={GlobalImageStyles.headerIcon} source={Images.goBack} /></Pressable>
        </View>
        <ScrollView style={Styles.scroll}>
          <Text style={Styles.boldText}>
            Privacy Policy for Kids Learning
          </Text>
          <Text style={Styles.normalText}>
            At Kids Learning, accessible from 
            <Text style={{color:'blue',}} onPress={() => Linking.openURL('https://colaninfotech.com/')}> colaninfotech
              </Text> one of our main
            priorities is the privacy of our visitors.
            This Privacy Policy document contains types of information that is collected
            and recorded by Kids Learning and how we use it.
            If you have additional questions or require more
            information about our Privacy Policy, do not hesitate to contact us.
          </Text>
          <Text style={Styles.boldText}>
            Log Files
          </Text>
          <Text style={Styles.normalText}>
            Kids Learning follows a standard procedure of using log files.
            These files log visitors when they visit websites.
            All hosting companies do this and a part of hosting services' analytics.
            The information collected by log files include internet protocol (IP)
            addresses, browser type, Internet Service Provider (ISP), date and time stamp,
            referring/exit pages, and possibly the number of clicks.
          </Text>
          <Text style={Styles.normalText}>
            These are not linked to any information that is personally identifiable.
            The purpose of the information is for analyzing trends, administering the site,
            tracking users' movement on the website, and gathering demographic information.
            Our Privacy Policy was created with the help of the Privacy Policy Generator.
          </Text>
          <Text style={Styles.boldText}>
            Cookies and Web Beacons
          </Text>
          <Text style={Styles.normalText}>
            Like any other website, Kids Learning uses 'cookies'.
            These cookies are used to store information including visitors'
            preferences, and the pages on the website that the visitor accessed or visited.
            The information is used to optimize the users' experience by customizing our
            web page content based on visitors' browser type and/or other information.
          </Text>
          <Text style={Styles.boldText}>
            Privacy Policies
          </Text>
          <Text style={Styles.normalText}>
            You may consult this list to find the Privacy Policy for each of the
            advertising partners of Kids Learning.
          </Text>
          <Text style={Styles.normalText}>
            Third-party ad servers or ad networks uses technologies like cookies,
            JavaScript, or Web Beacons that are used in their respective advertisements and
            links that appear on Kids Learning, which are sent directly to users' browser.
            They automatically receive your IP address when this occurs.
            These technologies are used to measure the effectiveness of their advertising
            campaigns and/or to personalize the advertising content that you see on websites
            that you visit.
          </Text>
          <Text style={Styles.boldText}>
            Third Party Privacy Policies
          </Text>
          <Text style={Styles.normalText}>
            Kids Learning's Privacy Policy does not apply to other advertisers or websites.
            Thus, we are advising you to consult the respective Privacy Policies of these
            third-party ad servers for more detailed information. It may include their practices and
            instructions about how to opt-out of certain options.
          </Text>
          <Text style={Styles.normalText}>
            You can choose to disable cookies through your individual browser options.
            To know more detailed information about cookie management with specific web browsers,
            it can be found at the browsers' respective websites. What Are Cookies?
          </Text>
          <Text style={Styles.boldText}>
            Children's Information
          </Text>
          <Text style={Styles.normalText}>
            Another part of our priority is adding protection for children while using the internet.
            We encourage parents and guardians to observe, participate in, and/or monitor and guide
            their online activity.
          </Text>
          <Text style={Styles.normalText}>
            Kids Learning does not knowingly collect any Personal Identifiable Information from children
            under the age of 13. If you think that your child provided this kind of information on
            our website, we strongly encourage you to contact us immediately and we will do our best
            efforts to promptly remove such information from our records.
          </Text>
          <Text style={Styles.boldText}>
            Online Privacy Policy Only
          </Text>
          <Text style={Styles.normalText}>
            This Privacy Policy applies only to our online activities and is valid for visitors to
            our website with regards to the information that they shared and/or collect in Kids Learning.
            This policy is not applicable to any information
            collected offline or via channels other than this website.
          </Text>
          <Text style={Styles.boldText}>
            Consent
          </Text>
          <Text style={Styles.normalText}>
            By using our website, you hereby consent to our Privacy Policy and agree to its Terms and Conditions.
          </Text>
        </ScrollView>
        <View style={Styles.bottom} />
        <Modal
          transparent
          animationType='fade'
          visible={modalVisible}
          onRequestClose={() => {
            setModalVisible(!modalVisible);
          }}
        >
          <Pressable style={Styles.centeredView}>
            <CustomNavigation changeModal={() => { setModalVisible(false) }}
              closeMenu={() => setModalVisible(false)} />
          </Pressable>
        </Modal>
      </ImageBackground>
    </View>
  )
}

export default PrivacyPolicy
