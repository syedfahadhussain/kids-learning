export default Styles = ({
    container: {
        flex: 1,
    },
    headerView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        // flex: 0.15,
        ...Platform.select({
            ios: {
                flex: 0.15,
                // backgroundColor:'#000',
                marginTop:'6%'
            },
            android: {
                flex: 0.15,
            },
        }),
        alignItems: 'center',
        marginHorizontal: 5,
    },
    headerText: {
        fontSize: 24,
        color: Colors.Header_Text,
        fontFamily: 'Poppins-SemiBold',
    },
    scroll: {
        flex: 0.4,
        marginTop: '2%',
        marginHorizontal: '5%',
    },
    bottom: {
        flex: 0.5,
    },
    boldText: {
        color: "#00243E",
        fontFamily: 'Poppins-Bold',
        fontSize: 16,
        // textAlign:'center'
    },
    normalText: {
        color: "#00243E",
        fontFamily: 'Poppins-Regular',
        fontWeight:'400',
        fontSize: 14,
        marginBottom: '1%'
    },
    webText: {
        color: "#00243E",
        fontFamily: 'Poppins',
        backgroundColor: '#221',
        // fontSize: 14,
        // marginBottom: '1%'
    },
    centeredView: {
        flex: 1,
        backgroundColor: Colors.Transparent,
        justifyContent: "center",
    },
})