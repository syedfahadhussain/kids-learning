import { View, Text, ImageBackground, Pressable, Image, Modal, ScrollView, BackHandler } from 'react-native'
import React, { useState, useEffect } from 'react'
import Images from '../../../res/images/Images'
import Styles from './Styles'
import CustomNavigation from '../../../global/topNavigation/CustomNavigation'
import { useNavigation } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage'

const AboutScreen = () => {
  navigation = useNavigation();

  useEffect(() => {
    const backAction = () => {
      navigation.replace('HomeScreen');
      return true;
    }

    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backAction,
    );

    return () => backHandler.remove();
  }, []);

  useEffect(() => {
    getData()
  }, [])
  const [name, setName] = useState('')


  const getData = () => {
    try {
      AsyncStorage.getItem('UserName')
      // console.log(name)
        .then(value => {
          if (value != null) {
            setName(value);
          }
        })
    } catch (error) {
      console.log(error);
    }
  }
  const [modalVisible, setModalVisible] = useState(false);
  return (
    <View style={Styles.container}>
      <ImageBackground style={Styles.container} resizeMode='stretch' source={Images.aboutBack}>
        <View style={Styles.headerView}>
          <Pressable onPress={() => setModalVisible(true)}>
            <Image style={GlobalImageStyles.headerIcon} source={Images.menuIcon} />
          </Pressable>
          <Text style={Styles.headerText}>About the App</Text>
          <Pressable onPress={() => { navigation.replace('HomeScreen') }}><Image style={GlobalImageStyles.headerIcon} source={Images.goBack} /></Pressable>
        </View>
        <ScrollView style={Styles.scroll}>
          <Text style={Styles.normalText}>
            Welcome to Kids Learning, Here we will provide you only interesting content, which you will like very much.
          </Text>
          <Text style={Styles.normalText}>
            Founded in 2022 by Colan, Colan Infotech has come a long way from its beginnings. We're working to turn our passion for into a booming. Have a nice day !
          </Text>
          <Text style={Styles.normalText}>We hope you enjoy our products as much as we enjoy offering them to you. If you have any questions or comments, please don't hesitate to contact us.</Text>
          <Text style={Styles.boldText}>Sincerely,
          </Text>
          <Text style={Styles.new} >Colan Infotech</Text>
        </ScrollView>
        <View style={{ flex: 0.5, }} />
        <Modal
          transparent
          animationType='fade'
          visible={modalVisible}
          onRequestClose={() => {
            setModalVisible(!modalVisible);
          }}
        >
          <Pressable style={Styles.centeredView}>
            <CustomNavigation changeModal={() => { setModalVisible(false) }}
              closeMenu={() => setModalVisible(false)} />
          </Pressable>
        </Modal>
      </ImageBackground>
    </View>
  )
}

export default AboutScreen