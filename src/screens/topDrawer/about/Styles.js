export default Style = ({
    container: {
        flex: 1,
    },
    headerView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        // flex: 0.15,
        ...Platform.select({
            ios: {
                flex: 0.15,
                // backgroundColor:'#000',
                marginTop:'6%'
            },
            android: {
                flex: 0.15,
            },
        }),
        alignItems: 'center',
        marginHorizontal: '1%',
    },
    headerText: {
        fontSize: 24,
        color: Colors.Header_Text,
        fontFamily: 'Poppins-SemiBold',
    },
    scroll: {
        flex: 0.4,
        marginTop: '2%',
        marginHorizontal: '5%',
    },
    centeredView: {
        flex: 1,
        backgroundColor: Colors.Transparent,
        justifyContent: "center",
    },
    bottom: {
        flex: 0.45,
    },
    boldText: {
        color: "#00243E",
        fontFamily: 'Poppins-SemiBold',
        fontSize: 20,
    },
    normalText: {
        color: "#00243E",
        fontFamily: 'Poppins-Medium',
        fontSize: 16,
        marginBottom: '10%',
    },
    new: {
        color: "#00243E",
        fontFamily: 'Poppins-Medium',
        fontSize: 14,
        marginBottom: '10%',
    },
})