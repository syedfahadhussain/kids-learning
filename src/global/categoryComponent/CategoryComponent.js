import { View, Text, ImageBackground, Pressable, Image, Modal, ScrollView, FlatList, BackHandler } from 'react-native'
import React, { useState, useEffect } from 'react'
import Styles from './Styles';
import Images from '../../res/images/Images';
import CustomNavigation from '../topNavigation/CustomNavigation';
import { useNavigation } from '@react-navigation/native';
import GlobalImageStyles from '../Styles/GlobalImageStyles';

const CategoryComponent = props => {
  navigation = useNavigation();
  const [modalVisible, setModalVisible] = useState(false);
  useEffect(() => {
    const backAction = () => {
      navigation.goBack(null);
      return true;
    }

    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backAction,
    );

    return () => backHandler.remove();
  }, []);

  return (
    <View style={Styles.container}>
      <ImageBackground style={Styles.container} source={Images.backgroundScreen}>
        <View style={Styles.headerView}>
          <Pressable onPress={() => setModalVisible(true)}>
            <Image style={GlobalImageStyles.headerIcon} source={Images.menuIcon} />
          </Pressable>
          <Text style={Styles.headerText}>{props.title}</Text>
          <Pressable onPress={() => { navigation.replace('HomeScreen') }}><Image style={GlobalImageStyles.headerIcon} source={Images.goBack} /></Pressable>
        </View>
        <ScrollView style={Styles.container}>
          <View style={Styles.contentView}>
            <View style={Styles.leftContent}>
              <Pressable style={{ marginBottom: 10 }} onPress={props.first}>{props.image}</Pressable>
              <Pressable onPress={props.third} >{props.image2}</Pressable>
            </View>

            <View style={Styles.rightContent}>
              <Pressable style={{ marginBottom: 10 }} onPress={props.second} >{props.image3}</Pressable>
              <Pressable onPress={props.fourth}>{props.image4}</Pressable>
            </View>
          </View>
          <View style={Styles.contentView}>
            <View style={Styles.leftContent}>
              <Pressable style={{ marginBottom: 10 }} onPress={props.fifth}>{props.image5}</Pressable>
              <Pressable onPress={props.seventh} >{props.image6}</Pressable>
            </View>

            <View style={Styles.rightContent}>
              <Pressable style={{ marginBottom: 10 }} onPress={props.sixth} >{props.image7}</Pressable>
              <Pressable onPress={props.eigth}>{props.image8}</Pressable>
            </View>
          </View>
          <View style={Styles.contentView}>
            <Pressable onPress={props.ninth} >{props.image9}</Pressable>
            <Pressable onPress={props.tenth}>{props.image10}</Pressable>
          </View>
        </ScrollView>
        <Modal
          transparent
          animationType='fade'
          visible={modalVisible}
          onRequestClose={() => {
            setModalVisible(!modalVisible);
          }}
        >
          <Pressable style={Styles.centeredView}>
            <CustomNavigation changeModal={() => { setModalVisible(false) }}
              closeMenu={() => setModalVisible(false)} />
          </Pressable>
        </Modal>
      </ImageBackground>
    </View>
  )
}

export default CategoryComponent