import Colors from "../../res/colors/Colors"
import { Platform } from "react-native"

export default Styles = ({
    container: {
        flex: 1,
    },
    headerView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        // flex: 0.1,
        ...Platform.select({
            ios: {
                flex: 0.1,
                // backgroundColor:'#000',
                marginTop:'6%'
            },
            android: {
                flex: 0.1,
            },
        }),
        alignItems: 'center',
        marginHorizontal: 5,
    },
    headerText: {
        fontSize: 24,
        color: Colors.Header_Text,
        fontFamily: 'Poppins-SemiBold',
    },
    contentView: {
        flexDirection: 'row',
        flex: 1,
        // alignItems: 'center',
        justifyContent: 'space-evenly',
        marginBottom: '5%'
    },
    rightTopContent: {
        flex: 0.5,
        backgroundColor: '#000',
        justifyContent: 'center',
        alignItems: 'center',
    },
    leftTopContent: {
        flex: 0.5,
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
    },
    rightBottomContent: {
        flex: 0.5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    leftBottomContent: {
        flex: 0.5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    centeredView: {
        flex: 1,
        backgroundColor: Colors.Transparent,
    },
    item: {
        backgroundColor: '#f9c2dc',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
    },
    title: {
        fontSize: 32,
    },
})