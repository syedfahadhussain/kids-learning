import { Dimensions } from 'react-native';

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;

export default GlobalImageStyles = ({
    bigImage: {
        resizeMode: 'stretch',
        height: Height / 100 * 48,
        width: Width / 100 * 45,
    },
    smallImage: {
        resizeMode: 'stretch',
        height: Height / 100 * 38,
        width: Width / 100 * 45,
    },
    homeImage: {
        resizeMode: 'contain',
        height: Height / 100 * 30,
        width: Width / 100 * 96,
    },
    treeBranch: {
        resizeMode: 'contain',
        width: Width / 100 * 100,
        height: Height / 100 * 22,
    },
    numberImage: {
        resizeMode: 'contain',
        height: Height / 100 * 12,
        width: Width / 100 * 22,
    },
    appleNumberImage: {
        resizeMode: 'contain',
        height: Height / 100 * 12,
        width: Width / 100 * 22,
    },
    tomatoNumberImage: {
        resizeMode: 'contain',
        height: Height / 100 * 10,
        width: Width / 100 * 20,
    },
    numberAnimals: {
        resizeMode: 'contain',
        height: Height / 100 * 12,
        width: Width / 100 * 25,
    },
    numberFiveImage: {
        resizeMode: 'contain',
        height: Height / 100 * 12,
        width: Width / 100 * 25,
    },
    brinjalNumImage: {
        resizeMode: 'contain',
        height: Height / 100 * 12,
        width: Width / 100 * 17,
    },
    elephantNumber: {
        resizeMode: 'stretch',
        height: Height / 100 * 15,
        width: Width / 100 * 30,
    },
    elephantImage: {
        resizeMode: 'contain',
        height: Height / 100 * 50,
        width: Width / 100 * 70,
    },
    colorImage: {
        resizeMode: 'contain',
        height: Height / 100 * 20,
        width: Width / 100 * 35,
    },
    eleColorImage: {
        resizeMode: 'stretch',
        height: Height / 100 * 20,
        width: Width / 100 * 45,
    },
    vehiclesColorImage: {
        resizeMode: 'contain',
        height: Height / 100 * 15,
        width: Width / 100 * 50,
    },
    colorImage2: {
        resizeMode: 'contain',
        height: Height / 100 * 17,
        width: Width / 100 * 35,
    },

    cardImage: {
        resizeMode: 'contain',
        height: Height / 100 * 7,
        width: Width / 100 * 80,
    },
    gifStyle: {
        resizeMode: 'contain',
        height: Height / 100 * 23,
        width: Width / 100 * 70,
    },
    gifKeepStyle: {
        resizeMode: 'contain',
        height: Height / 100 * 18,
        width: Width / 100 * 70,
    },
    modalImage: {
        resizeMode: 'contain',
        height: Height / 100 * 4,
        width: Width / 100 * 50,
    },
    modalIcon: {
        resizeMode: 'contain',
        height: Height / 100 * 7,
        width: Width / 100 * 18,
    },
    modalHomeIcon: {
        resizeMode: 'contain',
        height: Height / 100 * 8,
        width: Width / 100 * 18,
    },
    modalWinHomeIcon: {
        resizeMode: 'contain',
        height: Height / 100 * 13,
        width: Width / 100 * 25,
    },
    headerIcon: {
        resizeMode: 'contain',
        height: Height / 100 * 8,
        width: Width / 100 * 12,
    },
    loginImage: {
        resizeMode: 'contain',
        height: Height / 100 * 12,
        width: Width / 100 * 47,
        // backgroundColor: '#fff',
    },
    optionImage: {
        height: Height / 100 * 43,
        width: Width / 100 * 100,
    },
})