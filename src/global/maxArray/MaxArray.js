import { View, ImageBackground, Pressable, Image, Text } from 'react-native'
import React from 'react'
import Styles from './Styles'
import Images from '../../res/images/Images'
import { useNavigation } from '@react-navigation/native'

const MaxArray = props => {
  navigation = useNavigation();
  return (
    <View style={Styles.container}>
      <ImageBackground style={Styles.background} resizeMode='stretch' source={Images.categoryModal}>
        <View style={Styles.wooHooTextView}>
          <Text style={Styles.wooHooText}> WHO-HOO..! </Text>
        </View>
        <View style={Styles.goodTextView}>
          <Text style={Styles.goodText}>GOOD JOB</Text>
        </View>
        <View style={Styles.textView}>
          <Text style={Styles.text}>
            You have cleared all questions,
          </Text>
          <Text style={Styles.text}>
            Click OK to choose next category
          </Text>
        </View>
        <View style={Styles.icons} >
          <View style={{ flex: 0.1, }} />
          {/* <Pressable style={Styles.cancelView} onPress={props.cancel}>
            <Text style={Styles.cancelText}>Cancel</Text>
          </Pressable> */}
          <Pressable style={Styles.okView} onPress={props.category}>
            <Text style={Styles.okText}>OK</Text>
          </Pressable>
          <View style={{ flex: 0.1, }} />
        </View>
      </ImageBackground>
    </View>
  )
}

export default MaxArray