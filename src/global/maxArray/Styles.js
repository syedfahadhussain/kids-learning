import Colors from "../../res/colors/Colors"
import { Dimensions } from 'react-native';

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;

export default Styles = ({
  container: {
    flex: 1,
    backgroundColor: Colors.Transparent,
    justifyContent: 'center',
  },
  background: {
    flex: 0.4,
    marginHorizontal: '6%',
  },
  wooHooTextView: {
    flex: 0.16,
    alignItems: 'center',
    justifyContent: 'center',
  },
  wooHooText: {
    fontSize: 20,
    color: Colors.Modal_Text_Color,
    fontFamily: 'Poppins-Bold',
  },
  textView: {
    flex: 0.2,
  },
  text: {
    color: Colors.Modal_Text_Color,
    fontSize: 13,
    fontFamily: 'Poppins-Bold',
    textAlign: 'center',
    marginRight: '3%'
  },
  goodTextView: {
    flex: 0.3,
    justifyContent: 'center',
  },
  goodText: {
    color: Colors.Black,
    fontSize: 35,
    fontFamily: 'Poppins-Black',
    textAlign: 'center',
  },
  icons: {
    flex: 0.2,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  okView: {
    height: Height / 18,
    width: Width / 4,
    flex: 0.24,
    alignItems: 'center',
    backgroundColor: '#000',
    justifyContent: 'center',
    borderRadius: 15,
    marginRight: '3%'
  },
  okText: {
    fontSize: 14,
    fontFamily: 'Poppins-Bold',
    textAlign: 'center',
    color: '#fff'
  },
  cancelText: {
    fontSize: 14,
    fontFamily: 'Poppins-Bold',
    textAlign: 'center',
    color: '#fff'
  },
  cancelView: {
    flex: 0.24,
    height: Height / 18,
    width: Width / 4,
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.Modal_Text_Color,
  }
})