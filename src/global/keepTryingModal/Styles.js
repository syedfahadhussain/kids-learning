import Colors from "../../res/colors/Colors"

export default Styles = ({
  container: {
    flex: 1,
    backgroundColor: Colors.Transparent,
    justifyContent: 'center',
  },
  background: {
    flex: 0.5,
  },
  topImage: {
    flex: 0.1,

  },
  gifView: {
    flex: 0.5,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  gifImage: {
    flex: 0.15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textView: {
    flex: 0.16,
    marginHorizontal: '13%',
    justifyContent: 'center',
  },
  text: {
    color: Colors.Modal_Text_Color,
    fontSize: 14,
    fontFamily: 'Poppins-Bold',
    textAlign: 'center',
  },
  icons: {
    flex: 0.3,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    // backgroundColor: '#256',
    marginHorizontal: '12%',
  }
})