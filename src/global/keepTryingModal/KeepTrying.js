import { View, ImageBackground, Pressable, Image, Text } from 'react-native'
import React from 'react'
import Styles from './Styles'
import Images from '../../res/images/Images'
import { useNavigation } from '@react-navigation/native'
import GlobalImageStyles from '../Styles/GlobalImageStyles'

const KeepTrying = props => {
  navigation = useNavigation();
  return (
    <View style={Styles.container}>
      <ImageBackground style={Styles.background} resizeMode='stretch' source={Images.categoryModal}>
        <View style={Styles.topImage}>
          {/* <Image source={Images.closeImage} /> */}
        </View>
        <View style={Styles.gifView} >
          <Image style={GlobalImageStyles.gifKeepStyle} source={Images.keepTryingMod} />
        </View>
        <View style={Styles.gifImage}>
          <Image style={GlobalImageStyles.modalImage} source={Images.keepText} />
        </View>
        <View style={Styles.textView}>
          <Text style={Styles.text}>
            The secret of getting ahead is getting started.
          </Text>
        </View>

        <View style={Styles.icons}>
          <Pressable onPress={() => { navigation.replace('HomeScreen') }}>
            <Image style={GlobalImageStyles.modalHomeIcon} source={Images.homeIcon} />
          </Pressable>
          <Pressable onPress={props.closeModal}>
            <Image style={GlobalImageStyles.modalIcon} source={Images.retryIcon} />
          </Pressable>
          {/* <Pressable onPress={props.changeArrayValue} >
            <Image style={GlobalImageStyles.modalIcon} source={Images.nextIcon} />
          </Pressable> */}
        </View>
        <View style={{flex:0.1}}/>
      </ImageBackground>
    </View>
  )
}

export default KeepTrying