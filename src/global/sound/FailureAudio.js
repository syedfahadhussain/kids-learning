import React from 'react';
import Failure from '../../res/music/Failure.m4a'

var Sound = require('react-native-sound');


Sound.setCategory('Playback');


var tataan = new Sound(Failure,  (error) => {
  if (error) {
    console.log('failed to load the sound', error);
    return;
  }
  // if loaded successfully
  console.log('duration in seconds: ' + tataan.getDuration() + 'number of channels: ' + tataan.getNumberOfChannels());

});
export default async function FailureAudio() {
  tataan.play(success => {
    if (success) {
      console.log('successfully finished playing');
    } else {
      console.log('playback failed due to audio decoding errors');
    }
  });
};