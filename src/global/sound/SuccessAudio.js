import React from 'react';
import Success from '../../res/music/Success.m4a'

var Sound = require('react-native-sound');


Sound.setCategory('Playback');


var ding = new Sound(Success,  (error) => {
  if (error) {
    console.log('failed to load the sound', error);
    return;
  }
  // if loaded successfully
  console.log('duration in seconds: ' + ding.getDuration() + 'number of channels: ' + ding.getNumberOfChannels());

});

export default async function SuccessAudio() {
  ding.play(success => {
    if (success) {
      console.log('successfully finished playing');
    } else {
      console.log('playback failed due to audio decoding errors');
    }
  });
};
