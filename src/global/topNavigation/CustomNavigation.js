import { View, ImageBackground, Pressable, Image, Text, Animated, Alert, Modal } from 'react-native'
import React, { useState, } from 'react'
import Images from '../../res/images/Images'
import Styles from './Styles'
import Strings from '../../res/strings/Strings'
import { useNavigation } from '@react-navigation/native'
import LogoutAlert from '../../components/logoutAlert/LogoutAlert'

const About = () => {
  return (
    <View >
      <Image source={Images.about} />
      <Text style={Styles.aboutText}>{Strings.about}</Text>
    </View>
  )
}

const CustomNavigation = (props) => {
  const [Logout, setLogout] = useState(false)
  // const param = route.params.setModalVisible(false)

  const AboutNav = () => {
    props.changeModal(false)
    navigation.navigate('AboutScreen')
    props
  }

  const PrivacyNav = () => {
    props.changeModal(false)
    navigation.navigate('PrivacyPolicy')
  }

  const logOut = () => {
    [setLogout(true)]
  }

  navigation = useNavigation();
  return (
    <View style={Styles.customView}>
      <ImageBackground resizeMode='stretch' style={Styles.imageBackground} source={Images.homeModalBack}>
        <View style={Styles.topView}>
          <Text style={Styles.menuText}>{Strings.menu}</Text>
          <Pressable onPress={props.closeMenu}>
          <Image style={Styles.image} source={Images.closeImage} />
          </Pressable>
        </View>
        <View style={Styles.bottomView}>
          <Pressable onPress={() => AboutNav()} style={Styles.image}>
            <About /></Pressable>
          <Pressable onPress={() => PrivacyNav()} style={Styles.image}>
            <Image source={Images.privacyPolicy} /></Pressable>
          <Pressable onPress={() => logOut()}
            style={Styles.image}>
            <Image source={Images.logout} /></Pressable>
        </View>
      </ImageBackground>
      <Modal transparent
          visible={Logout}
          onRequestClose={() => {
            setLogout(!Logout);
          }}
        >
          <Pressable style={Styles.centeredView}>
            <LogoutAlert cancel={()=>{setLogout(false),props.changeModal(false)}}
                          logout={()=>{setLogout(false)}} />
          </Pressable>
        </Modal>
    </View>
  )
}

export default CustomNavigation