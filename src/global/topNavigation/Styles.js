import Colors from "../../res/colors/Colors"
import { Platform } from "react-native"

export default Styles = ({
    customView: {
        flex: 1,
    },
    imageBackground: {
        // flex: 0.3,
        ...Platform.select({
            ios: {
                flex: 0.33,
                // backgroundColor:'#000',
            },
            android: {
                flex: 0.3,
            },
        }),
    },
    topView: {
        // flex: 0.3,
        ...Platform.select({
            ios: {
                flex: 0.33,
                // backgroundColor:'#000',
                marginTop:'10%'
            },
            android: {
                flex: 0.3,
            },
        }),
        flexDirection: 'row',
        margin: 20,
        justifyContent: 'space-between',
    },
    menuText: {
        color: Colors.White,
        fontFamily: 'Poppins-SemiBold',
        fontSize: 24,
        alignSelf: 'center',
    },
    bottomView: {
        flex: 0.5,
        justifyContent: 'space-between',
        flexDirection: 'row',
        marginHorizontal: 18,
    },
    aboutText: {
        color: Colors.White,
        fontFamily: 'Poppins-SemiBold',
        alignSelf: 'center',
    },
    image: {
        alignSelf: 'center',
    },
    centeredView: {
        flex: 1,
        backgroundColor: Colors.Transparent,
        justifyContent: "center",
    },
})