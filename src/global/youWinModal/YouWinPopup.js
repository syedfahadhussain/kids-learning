import { View, ImageBackground, Pressable, Image, Text } from 'react-native'
import React from 'react'
import Styles from './Styles'
import Images from '../../res/images/Images'
import { useNavigation } from '@react-navigation/native'
import GlobalImageStyles from '../Styles/GlobalImageStyles'

const YouWinPopup = props => {
  navigation = useNavigation();

  const home = () => {
    navigation.replace('HomeScreen')
  }
  return (
    <View style={Styles.container}>

      <ImageBackground style={Styles.background} resizeMode='stretch' source={Images.categoryModal}>
        <View style={Styles.topImage}>
          {/* <Image style={GlobalImageStyles.headerIcon} source={Images.closeImage}/> */}
        </View>
        <View style={Styles.gifView} >
          <Image style={GlobalImageStyles.gifStyle} source={Images.youWin} />
        </View>
        <View style={Styles.youText}>
          <Image style={GlobalImageStyles.modalImage} source={Images.winText} />
        </View>
        <View style={Styles.icons}>

          {/* <Pressable onPress={props.closeModal}>
          <Image style={GlobalImageStyles.modalIcon} source={Images.retryIcon} />
          </Pressable> */}
          <Pressable onPress={() => { home() }}>
            <Image style={GlobalImageStyles.modalHomeIcon} source={Images.homeIcon} />
          </Pressable>
          <Pressable onPress={props.changeArrayValue} >
            <Image style={GlobalImageStyles.modalIcon} source={Images.nextIcon} />
          </Pressable>
        </View>
        <View style={{flex:0.1}}/>
      </ImageBackground>
    </View>
  )
}

export default YouWinPopup