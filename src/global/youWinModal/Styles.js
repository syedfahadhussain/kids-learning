import Colors from "../../res/colors/Colors"

export default Styles = ({
  container: {
    flex: 1,
    backgroundColor: Colors.Transparent,
    justifyContent: 'center',
  },
  background: {
    flex: 0.5,
  },
  topImage: {
    flex: 0.1,
  },
  gifView: {
    alignItems: 'center',
    flex: 0.6,
  },
  icons: {
      flex: 0.35,
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-evenly',
      marginHorizontal: '12%',
  },
  youText: {
    flex: 0.1,
    alignItems: 'center',
  }
})