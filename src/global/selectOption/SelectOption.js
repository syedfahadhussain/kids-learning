import { View, Text, Image, ImageBackground, Pressable, Modal, ScrollView } from 'react-native'
import React from 'react'
import Images from '../../res/images/Images'
import Styles from './Styles'
import CustomNavigation from '../topNavigation/CustomNavigation'
import GlobalImageStyles from '../Styles/GlobalImageStyles'

const SelectOption = props => {

  const [modalVisible, setModalVisible] = React.useState('false')
  return (
    <ImageBackground style={Styles.container} source={Images.backgroundScreen}>
      <View style={Styles.headerView}>
        <Pressable onPress={() => setModalVisible(true)}>
          <Image style={GlobalImageStyles.headerIcon} source={Images.menuIcon} />
        </Pressable>
        <Text style={Styles.headerText}>Select Options</Text>
        <Pressable onPress={() => { navigation.goBack('null') }}><Image style={GlobalImageStyles.headerIcon} source={Images.goBack} /></Pressable>
      </View>
      <ScrollView style={Styles.contentView}>
        <View style={Styles.contentView}>
          <Pressable onPress={props.onclick} style={Styles.number}>
            <Image style={GlobalImageStyles.optionImage} resizeMode='contain' source={Images.selectNumber} />
          </Pressable>
          <Pressable onPress={props.onclick2} style={Styles.color}>
            <Image style={GlobalImageStyles.optionImage} resizeMode='contain' source={Images.selectColor} />
          </Pressable>
        </View>
      </ScrollView>
      <Modal transparent
        animationType='fade'
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}
      >
        <Pressable style={Styles.container}>
          <CustomNavigation changeModal={() => { setModalVisible(false) }}
            closeMenu={() => setModalVisible(false)} />
        </Pressable>
      </Modal>
    </ImageBackground>

  )
}

export default SelectOption