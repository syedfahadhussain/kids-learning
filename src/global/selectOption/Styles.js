import Colors from "../../res/colors/Colors"
import { Platform } from "react-native"

export default Styles = ({
    container: {
        flex: 1,
        backgroundColor: Colors.Transparent,
    },
    headerView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        // flex: 0.1,
        ...Platform.select({
            ios: {
                flex: 0.1,
                // backgroundColor:'#000',
                marginTop:'6%'
            },
            android: {
                flex: 0.1,
            },
        }),
        alignItems: 'center',
        marginHorizontal: 5,
    },
    headerText: {
        fontSize: 24,
        color: Colors.Black,
        fontFamily: 'Poppins-SemiBold',
    },
    contentView: {
        flex: 1,
    },
    imageStyle: {
        resizeMode: 'stretch',
        alignSelf: 'center',
        justifyContent: 'center',
    },
    number: {
        flex: 0.4,
        marginBottom: 10,
    },
    color: {
        flex: 0.4,
    }
})