import Colors from "../../res/colors/Colors"
import { Dimensions } from 'react-native';

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;

export default Styles = ({
  container: {
    flex: 1,
    backgroundColor: Colors.Transparent,
    justifyContent: 'center',
  },
  background: {
    flex: 0.3,
    // backgroundColor: "blue",
    marginHorizontal: '10%',
  },
  oopsTextView: {
    flex: 0.16,
    alignItems: 'center',
    justifyContent: 'center',
  },
  oopsText: {
    fontSize: 20,
    color: Colors.Modal_Text_Color,
    fontFamily: 'Poppins-Bold',
  },
  textView: {
    flex: 0.3,
    justifyContent: 'center',
  },
  text: {
    color: Colors.Modal_Text_Color,
    fontSize: 16,
    fontFamily: 'Poppins-Bold',
    textAlign: 'center',
    marginRight: '3%'
  },
  goodTextView: {
    flex: 0.3,
    justifyContent: 'center',
  },
  goodText: {
    color: Colors.Black,
    fontSize: 35,
    fontFamily: 'Poppins-Black',
    textAlign: 'center',
  },
  icons: {
    flex: 0.2,
    alignItems: 'center',
  },
  okView: {
    height: Height / 14,
    width: Width / 4,
    alignItems: 'center',
    backgroundColor: Colors.Modal_Text_Color,
    justifyContent: 'center',
    borderRadius: 15,
    marginRight: '3%'
  },
  okText: {
    fontSize: 14,
    fontFamily: 'Poppins-Bold',
    textAlign: 'center',
    color: Colors.White
  },
})