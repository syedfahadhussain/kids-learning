import { View, ImageBackground, Pressable, Image, Text } from 'react-native'
import React from 'react'
import Styles from './Styles'
import Images from '../../res/images/Images'
import { useNavigation } from '@react-navigation/native'

const LoginAlert = props => {
  navigation = useNavigation();
  return (
    <View style={Styles.container}>
      <ImageBackground style={Styles.background} resizeMode='stretch' source={Images.categoryModal}>
        <View style={Styles.oopsTextView}>
          <Text style={Styles.oopsText}> OOPS..! </Text>
        </View>
        <View style={Styles.textView}>
          <Text style={Styles.text}>
            Please Enter your Nick Name
          </Text>
        </View>
        <View style={Styles.icons} >
          <Pressable style={Styles.okView} onPress={props.close}>
            <Text style={Styles.okText}>OK</Text>
          </Pressable>
        </View>
      </ImageBackground>
    </View>
  )
}

export default LoginAlert