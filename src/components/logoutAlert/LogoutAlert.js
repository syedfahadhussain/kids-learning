import { View, ImageBackground, Pressable, Image, Text ,} from 'react-native'
import React from 'react'
import Styles from './Styles'
import Images from '../../res/images/Images'
import { useNavigation } from '@react-navigation/native'
import AsyncStorage from '@react-native-async-storage/async-storage'

const LogoutAlert = props => {
  navigation = useNavigation();

  const logOut = () => {
    try {
      AsyncStorage.removeItem('UserName');
      navigation.replace('LoginScreen');
    }
    catch (e) {
      console.log('Error :==>' + e);
      navigation.replace('LoginScreen');
    }
  }
  return (
    <View style={Styles.container}>
      <ImageBackground style={Styles.background} resizeMode='stretch' source={Images.categoryModal}>
        <View style={Styles.alertTextView}>
          <Text style={Styles.alertText}>Alert</Text>
        </View>
        <View style={Styles.textView}>
          <Text style={Styles.text}>
            Are you sure want to logout?
          </Text>
        </View>
        <View style={Styles.icons} >
          <Pressable style={Styles.cancelView} onPress={props.cancel}>
            <Text style={Styles.cancelText} > No</Text>
          </Pressable>
          <Pressable style={Styles.okView} onPress={() => { [logOut(), props.logout] }}>
            <Text style={Styles.okText}> Yes</Text>
          </Pressable>
        </View>
      </ImageBackground>
    </View>
  )
}

export default LogoutAlert