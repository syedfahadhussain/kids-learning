import Colors from "../../res/colors/Colors"
import { Dimensions } from 'react-native';

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;

export default Styles = ({
  container: {
    flex: 1,
    backgroundColor: Colors.Transparent,
    justifyContent: 'center',
  },
  background: {
    flex: 0.3,
    // backgroundColor: "blue",
    marginHorizontal: '10%',
  },
  alertTextView: {
    flex: 0.16,
    alignItems: 'center',
    justifyContent: 'center',
  },
  alertText: {
    fontSize: 24,
    color: Colors.Modal_Text_Color,
    fontFamily: 'Poppins-Bold',
  },
  textView: {
    // flex: 0.3,
    ...Platform.select({
      ios: {
        flex: 0.4,
        marginHorizontal: '10%',
        // backgroundColor:'#000'
      },
      android: {
        flex: 0.3,
        marginHorizontal: '5%'
      },
  }),
    justifyContent: 'center',
    // marginHorizontal: '5%'
  },
  text: {
    color: Colors.Modal_Text_Color,
    fontSize: 20,
    fontFamily: 'Poppins-SemiBold',
    textAlign: 'center',
    marginRight: '3%'
  },
  icons: {
    flex: 0.3,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  okView: {
    height: Height / 18,
    width: Width / 4,
    alignItems: 'center',
    backgroundColor: Colors.Modal_Text_Color,
    justifyContent: 'center',
    borderRadius: 15,
    marginRight: '3%'
  },
  okText: {
    fontSize: 14,
    fontFamily: 'Poppins-Bold',
    textAlign: 'center',
    color: Colors.White
  },
  cancelView: {
    height: Height / 18,
    width: Width / 4,
    alignItems: 'center',
    backgroundColor: Colors.Black,
    justifyContent: 'center',
    borderRadius: 15,
    marginRight: '3%'
  },
  cancelText: {
    fontSize: 14,
    fontFamily: 'Poppins-Bold',
    textAlign: 'center',
    color: Colors.White
  },
})